import tensorflow as tf


class GaterAccuracy_tf2(tf.keras.metrics.Metric):

    def __init__(self, **kwargs):
        super(GaterAccuracy_tf2, self).__init__(name="gaterAcc", **kwargs)
        self.accuracy_obj = tf.keras.metrics.Accuracy(name="gaterAcc")

    @tf.function
    def update_state(self, y_true_and_gt_branch, y_pred, sample_weight=None):
        gt_branch = y_true_and_gt_branch[:, 1]
        y_pred_argmax = tf.argmax(y_pred, axis=-1)
        self.accuracy_obj.update_state(gt_branch, y_pred_argmax)

    def result(self):
        return self.accuracy_obj.result()

    def reset_states(self):
        self.accuracy_obj.reset_states()


def accuracySubtask_tf2(y_true_and_gt_branch, y_pred):
    y_true = y_true_and_gt_branch[:, 0]
    # gt_branch = y_true_and_gt_branch[:, 1]
    y_pred_argmax = tf.argmax(y_pred, axis=1)
    accuracy_obj = tf.keras.metrics.Accuracy(name="acc")
    accuracy_obj.update_state(y_true, y_pred_argmax)

    return accuracy_obj.result()

class AccuracySubtask_tf2(tf.keras.metrics.Metric):

    def __init__(self, stud_idx, **kwargs):
        super(AccuracySubtask_tf2, self).__init__(name="acc", **kwargs)
        self.accuracy_obj = tf.keras.metrics.Accuracy(name="acc")
        self.stud_idx = stud_idx

    @tf.function
    def update_state(self, y_true_and_gt_branch, y_pred, sample_weight=None):
        y_true = y_true_and_gt_branch[:, 0]
        gt_branch = y_true_and_gt_branch[:, 1]
        y_pred_argmax = tf.argmax(y_pred, axis=1)

        selected_branch = tf.where(gt_branch == self.stud_idx)
        selected_branch = tf.squeeze(selected_branch)
        if len(selected_branch) > 0:
            selected_y_true = tf.gather(y_true, selected_branch)
            selected_y_pred_argmax = tf.gather(y_pred_argmax, selected_branch)

            self.accuracy_obj.update_state(selected_y_true, selected_y_pred_argmax)
            # self.accuracy_obj.update_state(y_true, y_pred_argmax)

    def result(self):
        return self.accuracy_obj.result()

    def reset_states(self):
        self.accuracy_obj.reset_states()


class Accuracy_tf2(tf.keras.metrics.Metric):

    def __init__(self, **kwargs):
        super(Accuracy_tf2, self).__init__(name="acc", **kwargs)
        self.accuracy_obj = tf.keras.metrics.Accuracy(name="acc")

    @tf.function
    def update_state(self, y_true_and_gt_branch, y_pred, sample_weight=None):
        y_true = y_true_and_gt_branch[:, 0]
        y_pred_argmax = tf.argmax(y_pred, axis=-1)
        self.accuracy_obj.update_state(y_true, y_pred_argmax)

    def result(self):
        return self.accuracy_obj.result()

    def reset_states(self):
        self.accuracy_obj.reset_states()

import numpy as np
import tensorflow.keras as keras
from keras_preprocessing.image import ImageDataGenerator
from tensorflow.keras.datasets import cifar10, cifar100

from tools.de_utils import parse_food101
from tools.utils import get_image_cluster_indices
from preprocess_utils import cutout


def random_crop(img, random_crop_size=(32, 32)):
    height, width = img.shape[0], img.shape[1]
    dy, dx = random_crop_size
    x = np.random.randint(0, width - dx + 1)
    y = np.random.randint(0, height - dy + 1)
    return img[y:(y + dy), x:(x + dx), :]


class ImageLoader:
    def __init__(self, for_train, max_samples=None, ds_name="cifar10", use_res_and_crop=False,
                 to_categorical=False, coarse_label=False, preproc='mean', cluster_path=None, data_folder=""):
        assert ds_name in ["cifar10", "cifar100", "imagenet", "food101"]
        if "cifar" in ds_name:
            cifar = cifar10 if ds_name == "cifar10" else cifar100
            if for_train:
                (self.x, self.y), (_, _) = cifar.load_data()
                self.x_train = self.x
            else:
                (x_tr, _), (self.x, self.y) = cifar.load_data()
                self.x_train = x_tr

            if preproc == "mean":
                self.x_train_mean = np.mean(self.x_train.astype('float32') / 255.0, axis=0)
                self.x = self.x.astype('float32') / 255
                self.x -= self.x_train_mean
            elif preproc == "keras":
                from tensorflow.keras.applications.resnet50 import preprocess_input
                self.x = preprocess_input(self.x)

        elif "food101" in ds_name:
            if for_train:
                (self.x, self.y), (_, _) = parse_food101(img_size=32, custom_preprocess=False, to_categorical=to_categorical, data_folder=data_folder)
            else:
                (_, _), (self.x, self.y) = parse_food101(img_size=32, custom_preprocess=False, to_categorical=to_categorical, for_test_only=True, data_folder=data_folder)

        if max_samples is not None:
            self.x = self.x[:max_samples]
            self.y = self.y[:max_samples]

        if for_train:
            preprocessing_function = cutout if use_res_and_crop else None
            self.datagen = ImageDataGenerator(preprocessing_function=preprocessing_function,
                                              width_shift_range=0.1,
                                              height_shift_range=0.1,
                                              horizontal_flip=True,
                                              fill_mode='constant', cval=-1)
            self.datagen.fit(self.x)
        else:
            self.datagen = ImageDataGenerator()

        if coarse_label:

            cluster_indices = get_image_cluster_indices(cluster_path=cluster_path)
            if for_train:
                self.y = cluster_indices[:50000]
            else:
                self.y = cluster_indices[50000:]

        if to_categorical:
            self.y = keras.utils.to_categorical(np.squeeze(self.y), len(np.unique(self.y)))

    def generate_batches(self, batch_size, shuffle=True, multiply=1.0):
        nr_batches = int((len(self.y) * multiply) / batch_size)
        for batch_idx, (image_batch, y_batch) in enumerate(self.datagen.flow(self.x, self.y, batch_size=batch_size, shuffle=shuffle)):
            if batch_idx == nr_batches:
                break
            yield image_batch, y_batch, nr_batches
import json
import os
import sys
from abc import ABC
import argparse

sys.path.append(os.path.join(os.getcwd(), '..'))
sys.path.append(os.path.join(os.getcwd(), '../..'))

import tensorflow as tf
from tools.utils import get_num_studs, get_num_classes, make_sure_dirs_exists
from tools.loss_functions_hydra_tf1 import ImageSubtaskCrossentropyLoss, GateImageSubtaskLoss, StudentImageSubtaskLoss
from tools.ResSepModel_tf1 import resnetStem_tf1_unaltered, resnetAfterStem_tf1_unaltered
from metrics import GaterAccuracy_tf2, Accuracy_tf2
from HydraBaseModel import HydraNet_tf2, call_dynamic_K, call_static_K, call_backbone_and_gater

config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.compat.v1.Session(config=config)


def get_custom_stem(ds_size, input_size, depth, nr_stem_layers, do_wd_schedule, w_stem):
    result = None
    if ds_size == 32:
        result = resnetStem_tf1_unaltered(input_size, depth=depth,
                                          num_stem_layers=nr_stem_layers,
                                          do_wd_schedule=do_wd_schedule)

    return result


def get_custom_after_stem(ds_size, stem_network, depth=None, num_filters_stem=None, return_classification_only=False,
                          num_stem_layers=None, num_cls=0, do_wd_schedule=True, activation=None, w_branch=1.0,
                          dropout_rate=0.0):
    stud_model = None
    if ds_size == 32:
        stud_model = resnetAfterStem_tf1_unaltered(stem_network.outputs[0].shape[1:], depth, num_filters_stem,
                                                   num_stem_layers=num_stem_layers, total_nr_blocks=3,
                                                   num_classes=num_cls, activation=activation,
                                                   return_classification_only=return_classification_only,
                                                   do_wd_schedule=do_wd_schedule, dropout_rate=dropout_rate)
    return stud_model


class HydraOriginal_tf1_tf2(HydraNet_tf2, ABC):
    def __init__(self, seeds, num_student_classes=100, depth=8,
                 bs=32, optimizer_name="adam", ensemble_dropout=0.0, nr_stem_layers=2,
                 return_students=False, num_top_k=4, gater_activation=None,
                 return_projection=True, do_wd_schedule=False, k_threshold=0.0001, student_dropout=0.0, ds_size=32,
                 w_dict=None, gater_dropout=0.0, ds_name="food101"):

        super(HydraOriginal_tf1_tf2, self).__init__()

        if w_dict is None:
            w_dict = {
                "w_stem": 1,
                "w_branch": 1,
                "w_gater": 1
            }
        self.k_threshold = k_threshold
        self.ds_name = ds_name
        self.optimizer_name = optimizer_name
        self.do_wd_schedule = do_wd_schedule
        self.seeds = seeds
        self.return_projection = return_projection
        self.return_students = return_students
        self.depth = depth
        self.bs = bs
        self.num_top_k = num_top_k
        self.num_classes = num_student_classes
        self.nr_stem_layers = nr_stem_layers

        self.ensemble_dropout = ensemble_dropout
        self.student_dropout = student_dropout
        self.gater_dropout = gater_dropout

        self.students = []
        self.stem_network, num_filters_stem = get_custom_stem(ds_size, (ds_size, ds_size, 3), depth=depth,
                                                              nr_stem_layers=self.nr_stem_layers,
                                                              do_wd_schedule=do_wd_schedule, w_stem=w_dict['w_stem'])
        for s_idx, _ in enumerate(seeds):
            stud_num_cls = 0
            if self.return_students:
                stud_num_cls = self.num_classes
            stud_model = get_custom_after_stem(ds_size, self.stem_network, depth, num_filters_stem,
                                               num_stem_layers=self.nr_stem_layers, return_classification_only=False,
                                               num_cls=stud_num_cls, do_wd_schedule=do_wd_schedule,
                                               w_branch=w_dict['w_branch'], dropout_rate=self.student_dropout)
            self.students.append(stud_model)

        self.gater_model = get_custom_after_stem(ds_size, self.stem_network, depth,
                                                 num_filters_stem, num_stem_layers=self.nr_stem_layers,
                                                 num_cls=len(seeds), activation=gater_activation,
                                                 return_classification_only=True, do_wd_schedule=do_wd_schedule,
                                                 w_branch=w_dict['w_gater'], dropout_rate=self.gater_dropout)

        self.gap = tf.keras.layers.GlobalAveragePooling2D()
        if self.ensemble_dropout > 0.01:
            self.dropoutLayer = tf.keras.layers.Dropout(self.ensemble_dropout)
        self.combiner = tf.keras.layers.Conv2D(num_filters_stem, kernel_size=3, kernel_initializer='he_normal')
        self.classification_layer = tf.keras.layers.Dense(self.num_classes, activation=None,
                                                          kernel_initializer='he_normal')

    @tf.function
    def call_gater_predicted_branches_only(self, data, training=None):
        assert tf.shape(data)[0] == 1

        backbone = self.stem_network(data, training=training)
        gate_val = self.gater_model(backbone, training=training)
        v, k_indices = tf.math.top_k(gate_val, k=self.num_top_k)
        projections = []
        for selected_idx in k_indices[0]:
            stud_prediction = self.students[selected_idx](backbone, training=training)
            if self.return_students:
                stud_pr, stud_class = stud_prediction
            else:
                stud_pr = stud_prediction
            projections.append(stud_pr)

        student_selected = tf.stack(projections, axis=1)
        combiner = tf.reduce_sum(student_selected, axis=1)
        if hasattr(self, "combiner"):
            combiner = self.combiner(combiner, training=training)
        combiner_gap = self.gap(combiner)
        if self.ensemble_dropout >= 0.01:
            combiner_gap = self.dropoutLayer(combiner_gap, training=training)
        out = self.classification_layer(combiner_gap, training=training)

        return out

    @tf.function
    def call_top_k(self, data, num_top_k=4, training=None):

        backbone = self.stem_network(data, training=training)
        gate_val = self.gater_model(backbone, training=training)

        _, k_indices = tf.math.top_k(gate_val, k=num_top_k)
        one_hot_matrix = tf.reduce_sum(tf.one_hot(k_indices, depth=len(self.seeds)), axis=1)
        coordinates_selection = tf.where(one_hot_matrix)

        students_proj = []
        for stud_idx, student in enumerate(self.students):
            stud_out = student(backbone, training=training)
            if self.return_students:
                stud_proj, stud_class = stud_out
            else:
                stud_proj = stud_out
            students_proj.append(stud_proj)

        students_proj_stack = tf.stack(students_proj, axis=1)

        student_selected = tf.gather_nd(students_proj_stack, coordinates_selection)
        student_selected = tf.reshape(student_selected, [tf.shape(data)[0], num_top_k,
                                                         tf.shape(students_proj_stack)[2],
                                                         tf.shape(students_proj_stack)[3],
                                                         tf.shape(students_proj_stack)[4]])

        combiner = tf.reduce_sum(student_selected, axis=1)
        combiner_gap = self.gap(combiner)
        if self.ensemble_dropout >= 0.01:
            combiner_gap = self.dropoutLayer(combiner_gap, training=training)

        out = self.classification_layer(combiner_gap, training=training)
        final_output = (out, gate_val)
        final_output = final_output + tuple(students_proj)
        return final_output

    def multiply_lr(self, epoch, lr):
        if "cifar" in self.ds_name:
            if epoch > 180:
                lr *= 0.5e-3
            elif epoch > 160:
                lr *= 1e-3
            elif epoch > 120:
                lr *= 1e-2
            elif epoch > 80:
                lr *= 1e-1
        else:
            # food101
            if epoch > 90:
                lr *= 1e-3
            elif epoch > 80:
                lr *= 1e-2
            elif epoch > 60:
                lr *= 1e-1
        return lr

    def wd_schedule(self, epoch):
        wd = 1e-4
        wd = self.multiply_lr(epoch, wd)
        return wd

    def lr_schedule(self, epoch):
        lr = 1e-3
        lr = self.multiply_lr(epoch, lr)
        return lr


def set_the_right_call_method(dyn_top_k):
    if dyn_top_k:
        setattr(HydraOriginal_tf1_tf2, "call", call_dynamic_K)
    else:
        setattr(HydraOriginal_tf1_tf2, "call", call_static_K)
    setattr(HydraOriginal_tf1_tf2, "call_dynamic_K", call_dynamic_K)


def train_hydra_and_dynK(data_folder, k_threshold, ds_name, d_param, model):
    ds_size = 32
    subpartitioning_text = ""  # for food101 can be _cutout _random (the file are available in
    # tools/subtask_partitioning)

    num_students = get_num_studs(ds_name)
    num_classes = get_num_classes(ds_name)
    cluster_path_train = "../tools/subtask_partitioning/%s_effB7_%d_img_clusters_train%s.npy" % (
        ds_name, num_students, subpartitioning_text)
    cluster_path_test = "../tools/subtask_partitioning/%s_effB7_%d_img_clusters_test.npy" % (
        ds_name, num_students)
    seeds = [i + 1 for i in range(num_students)]

    cutout_patch = 0
    bs = 32
    epochs = 100 if ds_name == "food101" else 190
    preproc = "mean"
    gater_activation = "softmax"
    optimizer_name = "adam"

    stud_weight_classification = 0.01
    num_top_k = 4
    nr_stem_layers = 2
    do_wd_schedule = True
    save_best_only = True
    dropout = {
        "student": 0.0,
        "ensemble": 0.0,
        "gater": 0.0
    }

    gater_weight = 0.5
    if model == "hydrares":
        dyn_top_k = False
        return_students = False
    elif model == "dynk":
        dyn_top_k = True
        return_students = True
    else:
        print("model unknown")
        return -1

    set_the_right_call_method(dyn_top_k)
    depth = (d_param * 6) + 2
    tf.keras.backend.clear_session()
    base_checkpoint = "results/%s_%d/ret_stud_%d" % (ds_name, ds_size, return_students)
    checkpoint = os.path.join(base_checkpoint, "D%d_eps%d_studs%d_dynK%d_Wd%d_gW%.2f_bs%d" % (
        d_param, epochs, num_students, dyn_top_k, do_wd_schedule, gater_weight, bs))
    checkpoint = HydraOriginal_tf1_tf2.add_to_checkpoint(checkpoint, dropout["ensemble"], return_students,
                                                         dropout["student"], stud_weight_classification,
                                                         dropout["gater"])
    checkpoint += "_cutout%d%s" % (cutout_patch, subpartitioning_text)
    make_sure_dirs_exists(
        ["results", "results/%s_%d/" % (ds_name, ds_size), base_checkpoint.replace("wd", ""), base_checkpoint,
         checkpoint])
    multi_out_model = HydraOriginal_tf1_tf2(seeds, num_student_classes=num_classes, depth=depth,
                                            ensemble_dropout=dropout["ensemble"],
                                            student_dropout=dropout["student"],
                                            bs=bs, return_students=return_students,
                                            nr_stem_layers=nr_stem_layers,
                                            num_top_k=num_top_k, gater_activation=gater_activation,
                                            return_projection=False, optimizer_name=optimizer_name,
                                            do_wd_schedule=do_wd_schedule, k_threshold=k_threshold,
                                            ds_size=ds_size,
                                            gater_dropout=dropout["gater"], ds_name=ds_name)
    loss_fn = [ImageSubtaskCrossentropyLoss(from_logits=True, sparse=False, num_classes=num_classes),
               GateImageSubtaskLoss(from_logits=gater_activation is None)]
    metrics = [{"output_1": Accuracy_tf2()}, {"output_2": GaterAccuracy_tf2()}]
    loss_weights = [1, gater_weight]
    if return_students:
        metrics.extend({"output_%d" % (i + 3): None} for i in range(num_students))
        loss_fn.extend([StudentImageSubtaskLoss(stud_idx, name="cce_subtask_%d" % stud_idx) for stud_idx in
                        range(num_students)])
        loss_weights.extend([stud_weight_classification for _ in range(num_students)])
    monitor = "val_output_1_acc"
    loss_weights_str = ", ".join(["%.4f" % itm for itm in loss_weights])
    multi_out_model.write_config(checkpoint,
                                 extra_info="num_top_k %d, k_threshold %.4f "
                                            "dropout_%s, d_param_%d, nr_stem_layers%d, epochs %d, "
                                            "preproc_%s, cutout_%d, return_students_%d \n return_proj_%d gater_activation:"
                                            "%s\ncluster_path %s \n stud_weight_classification %.4f, "
                                            "stud_weight_zerorize%.5f\nloss weights %s\n " % (
                                                num_top_k, k_threshold, json.dumps(dropout), d_param,
                                                nr_stem_layers, epochs, preproc, cutout_patch, return_students,
                                                False, gater_activation, cluster_path_train,
                                                stud_weight_classification,
                                                0, loss_weights_str),
                                 dummy_shape=(2, ds_size, ds_size, 3))

    multi_out_model.run_train(loss_fn, loss_weights, checkpoint, monitor=monitor, ds_name=ds_name,
                              num_classes=num_classes, epochs=epochs,
                              subtract_pixel_mean=preproc == "mean", coarse_label=False,
                              early_stop_patience=None,
                              verbose=1, to_categorical=False, cluster_path_train=cluster_path_train,
                              cluster_path_test=cluster_path_test, index_in_input=True,
                              metrics=metrics, use_cut_out=cutout_patch > 0, save_best_only=save_best_only,
                              ds_size=ds_size, data_folder=data_folder)

    multi_out_model.write_hydra_results(checkpoint, ds_name, preproc, cluster_path_test, return_students,
                                        num_top_k=num_top_k, k_threshold=k_threshold, data_folder=data_folder)
    multi_out_model.write_topK_results(checkpoint, ds_name, preproc, k=3, data_folder=data_folder)
    multi_out_model.dump_weights_hydra(checkpoint, ds_name, preproc, activation=gater_activation,
                                       data_folder=data_folder)


def get_flops(depth, num_students):
    tf.compat.v1.reset_default_graph()
    session = tf.compat.v1.Session()
    graph = tf.compat.v1.get_default_graph()
    num_classes = 100
    seeds = [i + 1 for i in range(num_students)]

    with graph.as_default():
        with session.as_default() as sess:
            model = HydraOriginal_tf1_tf2(seeds, num_student_classes=num_classes, depth=depth, ensemble_dropout=0,
                                          bs=1, nr_stem_layers=2, num_top_k=4, gater_activation=None,
                                          return_students=True, return_projection=False)
            # ResSep hydra
            model.call(tf.random.uniform(shape=(1, 32, 32, 3)), training=False)
            # model.call_backbone_and_gater(tf.random.uniform(shape=(1, 32, 32, 3)), training=False)

            run_meta = tf.compat.v1.RunMetadata()
            opts = tf.compat.v1.profiler.ProfileOptionBuilder.float_operation()

            flops = tf.compat.v1.profiler.profile(graph=graph, run_meta=run_meta, cmd='op', options=opts)
            flps = flops.total_float_ops
            # model.call(tf.random.uniform(shape=(1, 32, 32, 3)), training=False)
            # params = model.count_params()
            params = 0
            return flps, params


def calculate_flops():
    tf.config.experimental_run_functions_eagerly(True)
    setattr(HydraOriginal_tf1_tf2, "call", call_static_K)
    setattr(HydraOriginal_tf1_tf2, "call_backbone_and_gater", call_backbone_and_gater)
    num_students = 4
    for d_param in [1, 2, 3, 4, 5, 6, 7, 9]:
        # for d_param in [1]:
        depth = (d_param * 6) + 2
        flps, params = get_flops(depth=depth, num_students=num_students)
        print("d %d: params %.2f \nflops %.2f" % (d_param, params / (10 ** 6), flps / (10 ** 6)))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-indir", "--data-folder",
                        help="used for training food-101, the folder should contain all the directories with images",
                        default="/data/image_databases/food-101/images32")
    parser.add_argument("-ds", "--ds-name",
                        help="ds name: [cifar100, food101]",
                        default="cifar100")
    parser.add_argument("-k", "--k",
                        help="the number of branches used for inference",
                        default=4)
    parser.add_argument("-k-th", "--k-threshold",
                        help="the threshold used for extra elimination, 0.01, 0.02, 0.04, 0.06 ... ",
                        default=0.01)
    parser.add_argument("-d", "--d-param",
                        help="the architecture multiplication paramtere",
                        default=1)
    parser.add_argument("-model", "--model",
                        help="can be hydrares or dynk",
                        default="hydrares")

    args = parser.parse_args()

    train_hydra_and_dynK(args.data_folder, args.k_threshold, args.ds_name, args.d_param, args.model)

import os
import pickle
import random

import numpy
import numpy as np
import tensorflow as tf


def reset_random_seeds(seed_value):
    os.environ['PYTHONHASHSEED'] = str(seed_value)
    tf.random.set_seed(seed_value)
    np.random.seed(seed_value)
    random.seed(seed_value)


def unpickle(file):
    with open(file, 'rb') as fo:
        dict = pickle.load(fo)
    return dict


def update_accuracy_and_precision(binary_prediction, accuracy_list, precision_list, count_of_match_one_list,
                                  count_one_match_a_zero_list, class_branch_map, y_batch, num_students):
    branch_gt = class_branch_map.lookup(tf.constant(y_batch, dtype=tf.int32))
    for active_student in range(num_students):
        selection = numpy.where(branch_gt == active_student)[0]
        non_selection = numpy.where(branch_gt != active_student)[0]
        y_true = np.zeros_like(y_batch)
        if len(selection) > 0:
            y_true[selection] = 1
        active_student_pred = numpy.argmax(binary_prediction[active_student], axis=1)

        count_of_match_one_list[active_student] += numpy.count_nonzero(
            y_true[selection] == active_student_pred[selection])
        count_one_match_a_zero_list[active_student] += numpy.count_nonzero(
            y_true[non_selection] != active_student_pred[non_selection])

        accuracy_list[active_student].update_state(y_true, active_student_pred)
        precision_list[active_student].update_state(y_true, active_student_pred)


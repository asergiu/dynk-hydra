import argparse
import os
import sys
from abc import ABC

sys.path.append(os.path.join(os.getcwd(), '..'))
sys.path.append(os.path.join(os.getcwd(), '../..'))

from tools.utils import get_num_classes, make_sure_dirs_exists
from tools.resnet20_keras_example import resnet_v1_unaltered, ressep_v1_unaltered

import tensorflow as tf
from tensorflow import keras
from HydraBaseModel import HydraNet_tf2

config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.compat.v1.Session(config=config)


class Individual_Resnet_tf2(HydraNet_tf2, ABC):
    def __init__(self, num_classes, depth, bs, optimizer_name="adam", activation=None, dropout_rate=None, res_sep=False,
                 do_wd_schedule=False, ds_name="food101"):
        super(Individual_Resnet_tf2, self).__init__()

        self.bs = bs
        self.ds_name = ds_name
        self.do_wd_schedule = do_wd_schedule
        self.optimizer_name = optimizer_name
        self.depth = depth
        if res_sep:
            self.model = ressep_v1_unaltered((32, 32, 3), depth, num_classes, activation=activation,
                                             dropout_rate=dropout_rate, do_wd_schedule=do_wd_schedule)
        else:
            self.model = resnet_v1_unaltered((32, 32, 3), depth, num_classes, activation=activation,
                                             dropout_rate=dropout_rate, do_wd_schedule=do_wd_schedule)

    def multiply_lr(self, epoch, lr):
        if "cifar" in self.ds_name:
            if epoch > 180:
                lr *= 0.5e-3
            elif epoch > 160:
                lr *= 1e-3
            elif epoch > 120:
                lr *= 1e-2
            elif epoch > 80:
                lr *= 1e-1
        else:
            # food101
            if epoch > 90:
                lr *= 1e-3
            elif epoch > 80:
                lr *= 1e-2
            elif epoch > 60:
                lr *= 1e-1
        return lr

    def wd_schedule(self, epoch):
        wd = 1e-4
        wd = self.multiply_lr(epoch, wd)
        return wd

    def lr_schedule(self, epoch):
        lr = 1e-3
        lr = self.multiply_lr(epoch, lr)
        return lr

    @tf.function
    def call(self, data, training=None):
        out = self.model(data, training=training)
        return out


def train_resnet(data_folder, ds_name, depth):
    # tf.config.experimental_run_functions_eagerly(True)

    activation = None
    dropout_rate = None
    ds_size = 32

    preproc = "mean"
    optimizer_name = "adam"

    num_classes = get_num_classes(ds_name)
    epochs = 100
    cutout_patch = 0
    bs = 32

    do_wd_schedule = True
    res_sep = False

    net_name = "ResSep" if res_sep else "Resnet"
    if res_sep:
        checkpoint_root = "results/%s_%d/resSep" % (ds_name, ds_size)
    else:
        checkpoint_root = "results/%s_%d/resnet/" % (ds_name, ds_size)


    keras.backend.clear_session()
    individual_model = Individual_Resnet_tf2(depth=depth, num_classes=num_classes, bs=bs,
                                             optimizer_name=optimizer_name, activation=activation,
                                             dropout_rate=dropout_rate, res_sep=res_sep,
                                             do_wd_schedule=do_wd_schedule, ds_name=ds_name)

    checkpoint = os.path.join(checkpoint_root, "%s_%s%d_eps%d_BS_%d_activation_%s_bottleneck" % (
    optimizer_name, net_name, depth, epochs, bs, str(activation)))

    if do_wd_schedule:
        checkpoint += "_WD_schedule"

    make_sure_dirs_exists(["results", "results/%s_%d/" % (ds_name, ds_size), "results/%s_%d/resnet/" % (ds_name, ds_size), checkpoint_root])

    loss_fn = keras.losses.CategoricalCrossentropy(from_logits=activation is None)

    individual_model.write_config(checkpoint, extra_info="lr_eps original, cutout_patch%d " % cutout_patch)
    individual_model.run_train(loss_fn, None, checkpoint, monitor="val_accuracy", ds_name=ds_name,
                               metrics="accuracy", coarse_label=False, epochs=epochs, verbose=1,
                               to_categorical=True,
                               subtract_pixel_mean=preproc == "mean", early_stop_patience=None,
                               use_cut_out=cutout_patch > 0, save_best_only=True, data_folder=data_folder)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-indir", "--data-folder",
                        help="used for training food-101, the folder should contain all the directories with images",
                        default="/data/image_databases/food-101/images32")
    parser.add_argument("-ds", "--ds-name",
                        help="ds name: [cifar100, food101]",
                        default="cifar100")
    parser.add_argument("-depth", "--depth",
                        help="resnet depth [8, 14, 20, 26, 32, 56, 110, 164]",
                        default=8)

    args = parser.parse_args()
    train_resnet(args.data_folder, args.ds_name, args.depth)

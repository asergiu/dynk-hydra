import csv
import numpy as np
import os


def read_data(file_path):
    with open(file_path, "r") as read_file:
        content = csv.reader(read_file, delimiter=',')
        depths = []
        accuracies = {"Resnet": [], "HydraRes": [], "DynKHydraRes": []}
        flops = {"Resnet": [], "HydraRes": [], "DynKHydraRes": []}
        current_target = None
        for line_idx, line in enumerate(content):
            if line_idx == 0:
                depths = line
            else:
                if line[0].isalpha():
                    current_target = line[0]
                else:
                    accuracies[current_target].append(float(line[0]))
                    flops[current_target].append(int(line[1]))
        return depths, accuracies, flops


def plot_madds_accuracy_form_file(file_path):
    import matplotlib.pyplot as plt
    depths, accuracies, flops = read_data(file_path)

    fig = plt.figure()

    # colors = []
    ax = fig.add_subplot()
    color_map = {"Resnet": 'cornflowerblue', "HydraRes": 'royalblue', "DynKHydraRes": 'green'}

    vs_list = ["DynKHydraRes", "HydraRes"]
    for k in vs_list:
        xy = []
        for i in range(len(depths)):
            xy.append([accuracies[k][i], flops[k][i]])
        colors = [color_map[k] for _ in range(len(depths))]

        s = [64 for _ in colors]
        xy = np.asarray(xy, dtype=np.float32)

        ax.scatter(xy[:, 1], xy[:, 0], c=colors, s=s, label=k.replace("DynKHydraRes", "DynK-Hydra(our)"))

    plt.ylabel('Accuracy (%)', fontsize=16)
    plt.xlabel('Flops (x10^6)', fontsize=16)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)

    ax.legend(loc="center right", fontsize=16)
    ax.grid(True)
    plt.show()
    # plt.savefig(os.path.join("%s_vs_%s.png" % (vs_list[0], vs_list[1])))


if __name__ == "__main__":
    file_path1 = "madds_accuracy.csv"
    plot_madds_accuracy_form_file(file_path1)

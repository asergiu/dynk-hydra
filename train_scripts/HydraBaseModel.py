import os
import sys
from abc import ABC
from os import listdir
from os.path import isfile, join


sys.path.append(os.path.join(os.getcwd(), '..'))
sys.path.append(os.path.join(os.getcwd(), '../..'))

from tools.de_utils import parse_cifar, parse_food101
from tensorflow.keras.callbacks import Callback
from tensorflow.keras.optimizers import SGD
from tools.utils import get_accuracies, get_image_cluster_indices, make_sure_dir_exists
from img_loader import ImageLoader
from preprocess_utils import cutout
import tensorflow_addons as tfa
from tensorflow.python.keras import backend as K
from tensorflow.keras.callbacks import ReduceLROnPlateau, EarlyStopping, LearningRateScheduler
from tensorflow.keras.optimizers import Adam
from keras_preprocessing.image import ImageDataGenerator
from tensorflow.keras.callbacks import ModelCheckpoint
import numpy as np

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.applications.resnet50 import preprocess_input

class WeightDecayScheduler(Callback):
    """https://article.itxueyuan.com/Zo8Jqj"""

    def __init__(self, schedule, verbose=0):
        # super(WeightDecayScheduler, self).__init__()
        super(WeightDecayScheduler).__init__()
        self.schedule = schedule
        self.verbose = verbose

    def on_epoch_begin(self, epoch, logs=None):
        if not hasattr(self.model.optimizer, 'weight_decay'):
            raise ValueError('Optimizer must have a "weight_decay" attribute.')
        try:  # new API
            weight_decay = float(K.get_value(self.model.optimizer.weight_decay))
            weight_decay = self.schedule(epoch, weight_decay)
        except TypeError:  # Support for old API for backward compatibility
            weight_decay = self.schedule(epoch)
        if not isinstance(weight_decay, (float, np.float32, np.float64)):
            raise ValueError('The output of the "schedule" function '
                             'should be float.')
        K.set_value(self.model.optimizer.weight_decay, weight_decay)
        if self.verbose > 0:
            print('nEpoch %05d: WeightDecayScheduler reducing weight '
                  'decay to %s.' % (epoch + 1, weight_decay))

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        logs['weight_decay'] = K.get_value(self.model.optimizer.weight_decay)


class HydraNet_tf2(keras.Model, ABC):
    def __init__(self):
        super(HydraNet_tf2, self).__init__()

    def run_train(self, loss_fn, loss_weights, checkpoint_path, monitor, ds_name, metrics='accuracy',
                  coarse_label=False,
                  epochs=180, num_classes=100, subtract_pixel_mean=True, early_stop_patience=10, verbose=0,
                  use_cut_out=True, index_in_input=False, to_categorical=True, cluster_path_train=None,
                  cluster_path_test=None,
                  base_ckp_name="val_acc", save_best_only=False, ds_size=32, data_folder=""):

        optimizer = Adam(lr=self.lr_schedule(0))
        if hasattr(self, "optimizer_name"):
            if self.optimizer_name == "sgd":
                optimizer = SGD(lr=self.lr_schedule(0), momentum=0.9, decay=0.0001)
            else:
                if hasattr(self, "do_wd_schedule") and self.do_wd_schedule:
                    optimizer = tfa.optimizers.AdamW(learning_rate=self.lr_schedule(0),
                                                     weight_decay=self.wd_schedule(0))
                else:
                    optimizer = Adam(lr=self.lr_schedule(0))

        self.compile(loss=loss_fn, loss_weights=loss_weights, optimizer=optimizer, metrics=metrics)

        (x_train, y_train), (x_test, y_test) = self.get_dataset(ds_name,
                                                                to_categorical=to_categorical, num_classes=num_classes,
                                                                coarse_label=coarse_label,
                                                                subtract_pixel_mean=subtract_pixel_mean, img_size=ds_size,
                                                                data_folder=data_folder)

        steps_per_epoch = x_train.shape[0] / self.bs

        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=checkpoint_path)

        model_checkpoint = ModelCheckpoint(
            filepath=os.path.join(checkpoint_path, base_ckp_name + ".{epoch:01d}--{" + monitor + ":.4f}.h5"),
            monitor=monitor, verbose=1, save_best_only=save_best_only, save_weights_only=True)

        callbacks = [model_checkpoint, tensorboard_callback]
        if hasattr(self, "do_wd_schedule") and self.do_wd_schedule:
            wd_callback = WeightDecayScheduler(self.wd_schedule)
            callbacks.append(wd_callback)
        lr_scheduler = LearningRateScheduler(self.lr_schedule)
        lr_reducer = ReduceLROnPlateau(factor=np.sqrt(0.1), cooldown=0, patience=5, min_lr=0.5e-6)
        if early_stop_patience is not None:
            early_stop = EarlyStopping(mode='min', verbose=1, patience=early_stop_patience, restore_best_weights=True)
            callbacks.append(early_stop)
        callbacks.extend([lr_reducer, lr_scheduler])
        preproc = cutout if use_cut_out else None
        datagen = ImageDataGenerator(preprocessing_function=preproc, width_shift_range=0.15, height_shift_range=0.15,
                                     horizontal_flip=True)
        datagen.fit(x_train)

        if index_in_input:
            print("getting cluster indices ...")
            train_cluster_indices = get_image_cluster_indices(cluster_path=cluster_path_train)
            test_cluster_indices = get_image_cluster_indices(cluster_path=cluster_path_test)
            y_train = np.stack((np.squeeze(y_train).astype(np.int32), train_cluster_indices), axis=1)
            y_test = np.stack((np.squeeze(y_test).astype(np.int32), test_cluster_indices), axis=1)

        print("start fit ...")
        self.fit(x=datagen.flow(x_train, y_train, batch_size=self.bs), y=None,
                 validation_data=(x_test, y_test),
                 epochs=epochs, verbose=verbose, workers=4,
                 callbacks=callbacks, steps_per_epoch=steps_per_epoch)
        self.save_weights(os.path.join(checkpoint_path, "final_epoch.h5"))

    def add_dropout_name(self, ckpt, val, name):
        if val is None:
            return ckpt
        return ckpt + name + "%.1f" % val

    def write_topK_results(self, checkpoint_path, ds_name, preproc, k, data_folder):
        test_on_train = False
        testing_ds = ImageLoader(test_on_train, max_samples=None, ds_name=ds_name, preproc=preproc, data_folder=data_folder)

        self.load_best_checkpoints(checkpoint_path)
        non_selected_nonZeros = []
        selected_nonZeros = []
        bs = 250
        best_selected_accuracy = tf.keras.metrics.Accuracy()
        for b_idx, (image_batch, y_batch, nr_batches) in enumerate(testing_ds.generate_batches(bs, shuffle=False)):
            if b_idx % 10 == 0:
                print(b_idx, "/", nr_batches)
            y_batch = np.squeeze(y_batch)
            all_pred = self.call_top_k(image_batch, num_top_k=k, training=False)

            best_prediction_logits = all_pred[0]

            k_values, k_indices = tf.math.top_k(all_pred[1], k=1)
            k_indices = np.squeeze(k_indices)
            projections = tf.stack(all_pred[2:], axis=1)

            for img_idx in range(len(image_batch)):
                img_proj = projections[img_idx]
                img_proj = np.reshape(img_proj,
                                      (img_proj.shape[0], img_proj.shape[1] * img_proj.shape[2] * img_proj.shape[3]))

                non_zero_count_proj = np.count_nonzero((img_proj > 0.0001).astype(int), axis=1)
                mean_without_selected = (np.sum(non_zero_count_proj) - non_zero_count_proj[k_indices[img_idx]]) / (
                        len(non_zero_count_proj) - 1)

                non_selected_nonZeros.append(mean_without_selected)
                selected_nonZeros.append(non_zero_count_proj[k_indices[img_idx]])

            best_prediction_classes = tf.argmax(best_prediction_logits, axis=-1).numpy()
            best_selected_accuracy.update_state(y_batch, best_prediction_classes)

        non_selected_nonZeros = np.asarray(non_selected_nonZeros)
        selected_nonZeros = np.asarray(selected_nonZeros)

        output_path = os.path.join(checkpoint_path, "result-best.txt")
        with open(output_path, 'a') as w_file:
            w_file.write("\ntop%d_selected accuracy %.4f" % (k, best_selected_accuracy.result().numpy()))
            w_file.write("\nnon_selection, non_zero_mean %.4f" % non_selected_nonZeros.mean())
            w_file.write("\nselection, non_zero_mean %.4f" % selected_nonZeros.mean())


    def write_hydra_results(self, checkpoint_path, ds_name, preproc, cluster_path_test, return_students, num_top_k=4,
                            k_threshold=0.01, data_folder=""):
        test_on_train = False
        testing_ds = ImageLoader(for_train=test_on_train, max_samples=None, ds_name=ds_name, preproc=preproc,data_folder=data_folder)

        self.load_best_checkpoints(checkpoint_path)

        cluster_indices = get_image_cluster_indices(cluster_path=cluster_path_test)

        num_students = len(np.unique(cluster_indices))
        output_path = os.path.join(checkpoint_path, "result-best.txt")
        bs = 250
        w_accuracy = tf.keras.metrics.Accuracy()
        ens_accuracy = tf.keras.metrics.Accuracy()
        subtask_accuracies = [tf.keras.metrics.Accuracy() for _ in range(num_students)]
        selected_count_list = []

        for b_idx, (image_batch, y_batch, nr_batches) in enumerate(testing_ds.generate_batches(bs, shuffle=False)):
            if b_idx % 10 == 0:
                print(b_idx, "/", nr_batches)
            y_batch = np.squeeze(y_batch)

            img_ds_idx_start = b_idx * bs
            img_ds_idx_end = (b_idx + 1) * bs
            gt_clusters = cluster_indices[img_ds_idx_start: img_ds_idx_end]

            all_pred = self(image_batch, training=False)

            ens_prediction = all_pred[0].numpy()
            weights = all_pred[1].numpy()
            top_k_values, top_k_indices = tf.math.top_k(all_pred[1], k=num_top_k)
            selected_count = np.sum(top_k_values.numpy() > k_threshold, axis=1).tolist()
            selected_count_list.extend(selected_count)

            if return_students:
                students_pred = all_pred[2:2 + num_students]
                students_pred_logits = tf.keras.backend.softmax(tf.stack(students_pred, axis=1)).numpy()
                students_pred_classes = tf.argmax(students_pred_logits, axis=-1).numpy()
                for img_idx, y_img in enumerate(y_batch):
                    gt_cluster = gt_clusters[img_idx]
                    subtask_accuracies[gt_cluster].update_state(y_img, students_pred_classes[img_idx][gt_cluster])

            weights_pred = np.argmax(weights, axis=1)
            ens_classification = np.argmax(ens_prediction, axis=1)
            ens_accuracy.update_state(y_batch, ens_classification)
            w_accuracy.update_state(gt_clusters, weights_pred)
        if return_students:
            str_subtask_acc = "[" + ", ".join(["%.4f" % acc.result() for i, acc in enumerate(subtask_accuracies)]) + "]"
            mean_acc = np.mean([acc.result() for acc in subtask_accuracies])
        selected_count_mean = np.array(selected_count_list).mean()

        with open(output_path, 'a') as w_file:
            w_file.write("\n\nk_threshold %.4f" % k_threshold)
            w_file.write("\nnum_top_k %d" % num_top_k)
            w_file.write("\nEns: %.4f" % ens_accuracy.result().numpy())
            w_file.write("\nGater: %.4f" % w_accuracy.result().numpy())
            w_file.write("\nSelected Branches Count Mean: %.4f" % selected_count_mean)
            if return_students:
                w_file.write("\nMean subtask acc %.4f" % mean_acc)
                w_file.write("\nsubtask acc %s" % str_subtask_acc)
        print("k_threshold", k_threshold)
        print("ens_accuracy", ens_accuracy.result().numpy())

    def dump_weights_hydra(self, checkpoint_path, ds_name, preproc, activation="softmax", data_folder=""):
        testing_ds = ImageLoader(for_train=False, max_samples=None, ds_name=ds_name, preproc=preproc,data_folder=data_folder)
        self.load_best_checkpoints(checkpoint_path)
        output_path = os.path.join(checkpoint_path, "dump_w.txt")
        with open(output_path, 'w') as outfile:
            for b_idx, (image_batch, y_batch, nr_batches) in enumerate(testing_ds.generate_batches(100, shuffle=False)):

                if b_idx % 10 == 0:
                    print(b_idx, "/", nr_batches)

                all_pred = self(image_batch, training=False)
                ens_prediction = all_pred[0]
                weights = all_pred[1]
                if activation is None:
                    weights = tf.keras.backend.softmax(weights)

                for img_idx in range(0, len(y_batch)):
                    gt = y_batch[img_idx]
                    pred_argmax = np.argmax(ens_prediction[img_idx].numpy())

                    sample_data = ''
                    sample_data += 'gt:' + str(gt) + "p:" + str(pred_argmax) + ","
                    max_w = tf.reduce_max(weights[img_idx])
                    for w_idx in range(len(weights[0])):
                        sample_data += '[w:' + "%.3f" % weights[img_idx][w_idx] + '], '

                    correct_ens = (pred_argmax == gt)
                    sample_data += ' E:' + str(correct_ens) + '(' + "%.3f" % max_w + ')'
                    sample_data += "\n"
                    outfile.write(sample_data)

    def dump_weights(self, checkpoint_path, ds_name, preproc,data_folder=""):
        testing_ds = ImageLoader(False, max_samples=None, ds_name=ds_name, preproc=preproc, data_folder=data_folder)
        self.load_best_checkpoints(os.path.dirname(checkpoint_path))
        with open(checkpoint_path, 'w') as outfile:
            for b_idx, (image_batch, y_batch, nr_batches) in enumerate(testing_ds.generate_batches(250, shuffle=False)):

                if b_idx % 10 == 0:
                    print(b_idx, "/", nr_batches)

                all_pred = self(image_batch, training=False)
                weights = all_pred[-1]
                ens_prediction = all_pred[-2]
                students_pred = all_pred[:-2]

                for img_idx in range(0, len(y_batch)):
                    gt = y_batch[img_idx]
                    sample_data = ''
                    sample_data += 'GT:' + str(gt) + ','

                    for model_idx in range(len(students_pred)):
                        p = students_pred[model_idx][img_idx].numpy()
                        correct = (np.argmax(p) == gt)
                        sample_data += '[w:' + "%.3f" % weights[img_idx][model_idx] + ', ' + str(correct) + '], '

                    p = ens_prediction[img_idx].numpy()
                    correct_ens = (np.argmax(p) == gt)
                    sample_data += ' E:' + str(correct_ens) + ''
                    sample_data += "\n"
                    outfile.write(sample_data)

    def coverage_and_accuracies(self, ds_name, is_baseline, preproc, data_folder):
        print("### start evaluating ###\n")
        final_predictions = [[] for _ in range(len(self.seeds) + 1)]
        ys = []

        testing_ds = ImageLoader(False, max_samples=None, ds_name=ds_name, to_categorical=False, preproc=preproc, data_folder=data_folder)

        at_least_one_correct = 0
        total_samples = 0
        for image_batch, y_batch, _ in testing_ds.generate_batches(250, shuffle=False):
            if is_baseline:
                all_pred = self(image_batch, training=False)
                ens_prediction = all_pred[-1]
                students_pred = all_pred[:-1]
            else:
                all_pred = self(image_batch, training=False)
                ens_prediction = all_pred[-2]
                students_pred = all_pred[:-2]

            correct_indices = set()
            total_samples += len(y_batch)

            # coverage part
            for model_idx in range(len(students_pred)):
                pred = students_pred[model_idx]

                model_pred = np.argmax(pred, axis=1)
                model_pred = np.expand_dims(model_pred, 1)
                correct_idx = np.asarray(y_batch == model_pred).astype(np.int32)
                non_zero_indices = np.nonzero(correct_idx)[0]
                correct_indices.update(non_zero_indices)
            at_least_one_correct += len(correct_indices)

            #  eval part
            for model_idx in range(len(students_pred)):
                pred = students_pred[model_idx]
                final_predictions[model_idx].extend(np.argmax(pred, axis=1))

            final_predictions[-1].extend(np.argmax(ens_prediction, axis=1))
            ys.extend(np.squeeze(y_batch))
            # break

        coverage = at_least_one_correct / total_samples
        predictors_accuracies = get_accuracies(self.seeds, final_predictions, ys)
        return coverage, predictors_accuracies

    def eval_manual_selection(self, checkpoint_path, ds_name, k, eliminate_extra_class=False):

        self.load_best_checkpoints(checkpoint_path)
        testing_ds = ImageLoader(False, max_samples=None, ds_name=ds_name, to_categorical=False)

        correct_pred_in_top_k = 0
        top1_selection_from_gater = 0
        max_from_gater_selection = 0
        max_from_all = 0
        count_mean_ensemble = 0

        count_mean_ensemble_withNoExtraClass = 0
        max_from_gater_selection_withNoExtraClass = 0
        from_gater_selection__class100 = 0

        for b_idx, (image_batch, y_batch, nr_batches) in enumerate(testing_ds.generate_batches(250, shuffle=False)):
            if b_idx % 10 == 0:
                print(b_idx, "/", nr_batches)
            all_pred = self.call_raw(image_batch, training=False)
            gate_val = all_pred[-1]
            stud_prediction = tf.stack(all_pred[:-1], axis=1).numpy()

            _, k_indices = tf.math.top_k(gate_val, k=k)
            _, idx_best_selected_by_gater = tf.math.top_k(gate_val, k=1)
            idx_best_selected_by_gater = np.squeeze(idx_best_selected_by_gater)
            k_indices = k_indices.numpy()

            for img_idx in range(len(image_batch)):
                gt_cls = y_batch[img_idx][0]

                img_stud_preds = stud_prediction[img_idx]
                top1_selection_from_gater += int(
                    np.argmax(img_stud_preds[idx_best_selected_by_gater[img_idx]]) == gt_cls)

                max_from_all += int(np.argmax(np.max(keras.backend.softmax(img_stud_preds), axis=0)) == gt_cls)

                selected_img_stud_pred = img_stud_preds[k_indices[img_idx]]
                ensemble_prediction = np.argmax(np.mean(selected_img_stud_pred, axis=0))
                count_mean_ensemble += int(ensemble_prediction == gt_cls)
                if eliminate_extra_class:
                    ensemble_prediction_noExtraClass = np.argmax(np.mean(selected_img_stud_pred[:, :-1], axis=0))
                    count_mean_ensemble_withNoExtraClass += int(ensemble_prediction_noExtraClass == gt_cls)
                # selected_img_stud_pred_softmax = keras.backend.softmax(selected_img_stud_pred)
                selected_img_stud_pred_softmax = selected_img_stud_pred
                selected_cls_pred = np.argmax(selected_img_stud_pred_softmax, axis=1)

                if eliminate_extra_class:
                    from_gater_selection__class100 += np.count_nonzero(
                        np.argmax(selected_img_stud_pred_softmax, axis=1) == 100)
                    # if from_gater_selection__class100 > 0:
                    #     print("a")
                    most_decided_prediction_noExtraClass = np.argmax(
                        np.max(selected_img_stud_pred_softmax[:, :-1], axis=0))
                    max_from_gater_selection_withNoExtraClass += int(most_decided_prediction_noExtraClass == gt_cls)

                correct_pred_in_top_k += int(gt_cls in selected_cls_pred)

                most_decided_prediction = np.argmax(np.max(selected_img_stud_pred_softmax, axis=0))
                max_from_gater_selection += int(most_decided_prediction == gt_cls)
        # return
        print("correct_pred_in_top_k=%d" % k, correct_pred_in_top_k)
        print("top1_selection_from_gater", top1_selection_from_gater)
        print("max_from_gater_selectiontop_k=%d" % k, max_from_gater_selection)
        print("max_from_all", max_from_all)
        print("mean_ensemble", count_mean_ensemble)
        if eliminate_extra_class:
            print("max_from_gater_selection_withNoExtraClass", max_from_gater_selection_withNoExtraClass)
            print("from_gater_selection__class100", from_gater_selection__class100)
            print("count_mean_ensemble_withNoExtraClass", count_mean_ensemble_withNoExtraClass)

        out_file = os.path.join(checkpoint_path, "result-best.txt")
        with open(out_file, "a") as out_file:
            out_file.write("\n#### Results manual selection #####")
            out_file.write("\n correct_pred_in_top_%d %d" % (k, correct_pred_in_top_k))
            out_file.write("\n top1_selection_from_gater %d" % top1_selection_from_gater)
            out_file.write("\n max_from_gater_selection_top_%d %d" % (k, max_from_gater_selection))
            out_file.write("\n max_from_all %d" % max_from_all)
            out_file.write("\n mean_ensemble %d" % count_mean_ensemble)
            if eliminate_extra_class:
                out_file.write(
                    "\n max_from_gater_selection_withNoExtraClass %d" % max_from_gater_selection_withNoExtraClass)
                out_file.write("\n from_gater_selection__class100 %d" % from_gater_selection__class100)
                out_file.write("\n count_mean_ensemble_withNoExtraClass %d" % count_mean_ensemble_withNoExtraClass)
        # return correct_pred_in_top_k, top1_selection_from_gater,  max_from_gater_selection, max_from_all, count_mean_ensemble
        # exit(0)

    def load_final_checkpoints(self, dir_path):
        self(tf.random.uniform(shape=(2, 32, 32, 3)), training=False)  # h7ack to initialize the model
        self.load_weights(filepath=join(dir_path, "final_epoch.h5"), by_name=False)
        print("loaded weights from ", join(dir_path, "final_epoch.h5"))

    def load_best_checkpoints(self, dir_path, reverse=False):
        onlyfiles = [f for f in listdir(dir_path) if isfile(join(dir_path, f)) and ".h5" in f]
        if "final_epoch.h5" in onlyfiles:
            onlyfiles.remove("final_epoch.h5")
        onlyfiles = sorted(onlyfiles, reverse=reverse, key=lambda x: float(x.split("--")[1].replace(".h5", "")))
        if len(onlyfiles) > 0:
            best_path = onlyfiles[-1]
            # best_path = "val_acc.92--0.0101.h5"
            self(tf.random.uniform(shape=(2, 32, 32, 3)), training=False)  # h7ack to initialize the model
            self.load_weights(filepath=join(dir_path, best_path), by_name=False)
            print("loaded weights from ", join(dir_path, best_path))
        else:
            print("\nCANNOT LOAD THE MODEL FROM\n", dir_path)

    def analyse_ambiguity_baseline(self, checkpoint_path, num_students, out_path, preproc="mean"):
        ds_name = "cifar100"

        testing_ds = ImageLoader(False, max_samples=None, ds_name=ds_name, preproc=preproc)
        students_ambiguity = [[] for _ in range(num_students)]
        self(tf.zeros(shape=(1, 32, 32, 3)), training=False)

        self.load_best_checkpoints(checkpoint_path)

        for b_idx, (image_batch, y_batch, nr_batches) in enumerate(testing_ds.generate_batches(250, shuffle=False)):
            stud_pred_class = []  # this is a list of lists
            all_pred = self(image_batch, training=False)
            ensemble_pred = np.argmax(all_pred[-1].numpy(), axis=1)

            for stud_idx in range(0, num_students):
                students_pred = all_pred[stud_idx].numpy()
                stud_pred_class.append(np.argmax(students_pred, axis=1))

            for m_idx in range(num_students):
                non_matching_stud = (stud_pred_class[m_idx] != ensemble_pred).astype(np.int)
                ambiguity_stud = np.sum(non_matching_stud)
                students_ambiguity[m_idx].append(ambiguity_stud)

        with open(join(out_path, "baseline_diversity_summary.txt"), "w") as f:
            students_ambiguity_str = " ".join("%d" % item for item in np.asarray(students_ambiguity).sum(axis=1))
            print("students_ambiguity", students_ambiguity_str)
            print("ENSEMBLE ambiguity", np.asarray(students_ambiguity).sum())

            f.write("students ambiguity %s" % students_ambiguity_str)
            f.write("\nENSEMBLE ambiguity(sum) %d" % np.asarray(students_ambiguity).sum())

    def write_results(self, checkpoint_path, ds_name="cifar10", load_best_checkpoints=True, is_baseline=False,
                      extra_name="", preproc="mean", data_folder=""):
        make_sure_dir_exists(checkpoint_path)
        if load_best_checkpoints:
            self.load_best_checkpoints(checkpoint_path)
        coverage, validation_acc = self.coverage_and_accuracies(ds_name, is_baseline=is_baseline, preproc=preproc, data_folder=data_folder)
        out_file = os.path.join(checkpoint_path, "result-best.txt")
        str_acc = '\n'
        for i in range(len(validation_acc) - 1):
            str_acc += "%.4f," % validation_acc[i]
        str_acc += '\nE %.4f ' % validation_acc[len(validation_acc) - 1]
        print(str_acc)
        print("Coverage %.4f" % coverage)
        with open(out_file, "a") as out_file:
            out_file.write("\n#### Results %s #####" % extra_name)
            out_file.write("\nAll classes acc:")
            out_file.write(str_acc)
            out_file.write("\nCoverage %.4f" % coverage)

    @staticmethod
    def get_dataset(ds_name, to_categorical=False, num_classes=-1, coarse_label=False, subtract_pixel_mean=True,
                    is_kotys=False, img_size=32,data_folder=""):
        if "cifar" in ds_name:
            return parse_cifar(ds_name, to_categorical, num_classes, coarse_label, subtract_pixel_mean, None)
        if "food101" in ds_name:
            return parse_food101(img_size=img_size, custom_preprocess=None,
                                 to_categorical=to_categorical,data_folder=data_folder)

    @staticmethod
    def process_imgs(imgs, preprocess_type, mean=None):
        if preprocess_type == "mean":
            return (imgs.astype('float32') / 255) - mean
        elif preprocess_type == "keras":
            return preprocess_input(imgs)
        else:
            return None

    @staticmethod
    def add_to_checkpoint(checkpoint, ensemble_dropout, return_students, student_dropout, stud_weight_classification,
                          gater_dropout):
        if ensemble_dropout > 0.01:
            checkpoint += "_eDrop%.1f" % ensemble_dropout
        if gater_dropout > 0.01:
            checkpoint += "_gDrop%.1f" % gater_dropout
        if return_students:
            checkpoint += "_sDrop%.1f_studw%.2f" % (student_dropout, stud_weight_classification)
        return checkpoint

    def write_config(self, checkpoint_path, extra_info="", dummy_shape=(2, 32, 32, 3)):
        make_sure_dir_exists(checkpoint_path)
        out_file = os.path.join(checkpoint_path, "result-best.txt")
        with open(out_file, "a") as out_file:
            if hasattr(self, "layers_w") and self.layers_w is not None:
                layers_w_str = " ".join([str(l) for l in self.layers_w])
                out_file.write("layers_w: %s\n" % layers_w_str)
            if hasattr(self, "layers_student") and self.layers_student is not None:
                layers_w_str = " ".join([str(l) for l in self.layers_student])
                out_file.write("layers_student: %s\n" % layers_w_str)
            if hasattr(self, "pool_size"):
                out_file.write("pool_size %s\n" % self.pool_size)
            if hasattr(self, "w_activation"):
                out_file.write("gater_activation %s " % self.w_activation)
            if hasattr(self, "stud_activation"):
                out_file.write("stud_activation %s\n" % self.stud_activation)
            if hasattr(self, "return_students"):
                out_file.write("return all students %s\n" % self.return_students)
            if hasattr(self, "bi_resnet_bock_start"):
                out_file.write("\nbi_resnet_bock_start %s " % self.bi_resnet_bock_start)
            out_file.write("\nextra_info: " + extra_info)
            if hasattr(self, "nr_steam_layers"):
                out_file.write("\n nr_steam_layers %d" % self.nr_steam_layers)
            if hasattr(self, "num_top_k"):
                out_file.write("\n num_top_k %d" % self.num_top_k)
            if hasattr(self, "active_student") and self.active_student is not None:
                out_file.write("\n active_student %d" % self.active_student)
            if hasattr(self, "w_model_channels_coef") and self.w_model_channels_coef is not None:
                out_file.write("\n w_model_channels_coef: %d" % self.w_model_channels_coef)
            if hasattr(self, "ensemble_cce_w") and self.ensemble_cce_w is not None:
                out_file.write("\n ensemble_cce_w: %d" % self.ensemble_cce_w)
            if hasattr(self, "cce_studs_w"):
                out_file.write("\n cce_STUDENT_weights %s" % self.cce_studs_w)
            if hasattr(self, "gater_dropout"):
                out_file.write("\n gater_dropout: " + str(self.gater_dropout))
            if hasattr(self, "student_dropout"):
                out_file.write("\n student_dropout: " + str(self.student_dropout))
            if hasattr(self, "nr_stem_layers"):
                out_file.write("\n nr_stem_layers: " + str(self.nr_stem_layers))
            if hasattr(self, "do_wd_schedule"):
                out_file.write("\n do_wd_schedule: " + str(self.do_wd_schedule))
            # try:
            # empty_input = [tf.random.uniform(shape=(2, 32, 32, 3)), tf.zeros(shape=(2, 100))] if y_true_in_input else tf.zeros(shape=(2, 32, 32, 3))
            empty_input = tf.random.uniform(shape=dummy_shape)
            self(empty_input, training=False)
            out_file.write("\nTotal Params: %d" % self.count_params())
            if hasattr(self, "gate_model"):
                out_file.write("\nParams gate: %d" % self.gate_model.count_params())
            if hasattr(self, "gater_model"):
                out_file.write("\nParams gate: %d" % self.gater_model.count_params())
            if hasattr(self, "stem_network"):
                out_file.write("\nParams stem: %d" % self.stem_network.count_params())
            if hasattr(self, "students"):
                out_file.write("\nParams 1 student: %d" % self.students[0].count_params())
            # except:
            #     pass


@tf.function
def call_model_indices(self, data, training=None, indices=None):
    backbone = self.stem_network(data, training=training)
    students_proj = []

    for stud_idx in indices:
        stud_out = self.students[stud_idx](backbone, training=training)
        if self.return_students:
            stud_proj, stud_class = stud_out
        else:
            stud_proj = stud_out
        students_proj.append(stud_proj)

    students_stack = tf.stack(students_proj, axis=1)
    combiner = tf.reduce_sum(students_stack, axis=1)
    if hasattr(self, "combiner"):
        combiner = self.combiner(combiner, training=training)
    combiner_gap = self.gap(combiner)

    final_output = self.classification_layer(combiner_gap, training=training)

    return final_output


@tf.function
def call_dynamic_K(self, data, training=None):
    backbone = self.stem_network(data, training=training)
    gate_val = self.gater_model(backbone, training=training)

    k_max, k_indices = tf.math.top_k(gate_val, k=self.num_top_k)
    one_hot_gate_matrix = tf.where(gate_val <= self.k_threshold, tf.ones_like(gate_val) * -1, tf.zeros_like(gate_val))

    one_hot_matrix = tf.reduce_sum(tf.one_hot(k_indices, depth=len(self.seeds)), axis=1)

    one_hot_matrix_no_zeros = tf.math.add(one_hot_gate_matrix, one_hot_matrix)
    one_hot_matrix_no_zeros = tf.where(one_hot_matrix_no_zeros > 0, tf.ones_like(one_hot_matrix_no_zeros),
                                       tf.zeros_like(one_hot_matrix_no_zeros))

    students_proj = []
    students_classification = []
    for stud_idx, student in enumerate(self.students):
        stud_out = student(backbone, training=training)
        if self.return_students:
            stud_proj, stud_class = stud_out
            students_classification.append(stud_class)
        else:
            stud_proj = stud_out
        students_proj.append(stud_proj)

    students_stack = tf.stack(students_proj, axis=1)

    mask_one_hot_higher_dim = tf.ones_like(students_stack) * tf.expand_dims(
        tf.expand_dims(tf.expand_dims(one_hot_matrix_no_zeros, axis=-1), axis=-1), axis=-1)
    student_selected = tf.where(mask_one_hot_higher_dim > 0, students_stack, tf.zeros_like(students_stack))

    combiner = tf.reduce_sum(student_selected, axis=1)
    if hasattr(self, "combiner"):
        combiner = self.combiner(combiner, training=training)
    combiner_gap = self.gap(combiner)

    if self.ensemble_dropout:
        combiner_gap = self.dropoutLayer(combiner_gap, training=training)
    out = self.classification_layer(combiner_gap, training=training)

    final_output = (out, gate_val)

    if self.return_students:
        final_output = final_output + tuple(students_classification)

    if self.return_projection:
        final_output = final_output + tuple(students_proj)

    return final_output


@tf.function
def call_backbone_and_gater(self, data, training=None):
    backbone = self.stem_network(data, training=training)
    gate_val = self.gater_model(backbone, training=training)
    return backbone


@tf.function
def call_static_K(self, data, training=None):
    backbone = self.stem_network(data, training=training)
    gate_val = self.gater_model(backbone, training=training)

    _, k_indices = tf.math.top_k(gate_val, k=self.num_top_k)
    one_hot_matrix = tf.reduce_sum(tf.one_hot(k_indices, depth=len(self.seeds)), axis=1)
    coordinates_selection = tf.where(one_hot_matrix)

    students_proj = []
    students_classification = []
    for stud_idx, student in enumerate(self.students):
        stud_out = student(backbone, training=training)
        if self.return_students:
            stud_proj, stud_class = stud_out
            students_classification.append(stud_class)
        else:
            stud_proj = stud_out
        students_proj.append(stud_proj)

    students_proj_stack = tf.stack(students_proj, axis=1)

    student_selected = tf.gather_nd(students_proj_stack, coordinates_selection)
    student_selected = tf.reshape(student_selected,
                                  [tf.shape(data)[0], self.num_top_k, tf.shape(students_proj_stack)[2],
                                   tf.shape(students_proj_stack)[3], tf.shape(students_proj_stack)[4]])

    combiner = tf.reduce_sum(student_selected, axis=1)
    if hasattr(self, "combiner"):
        combiner = self.combiner(combiner, training=training)
    combiner_gap = self.gap(combiner)

    if self.ensemble_dropout:
        combiner_gap = self.dropoutLayer(combiner_gap, training=training)

    out = self.classification_layer(combiner_gap, training=training)
    final_output = (out, gate_val)

    if self.return_students:
        final_output = final_output + tuple(students_classification)

    if self.return_projection:
        final_output = final_output + tuple(students_proj)

    return final_output
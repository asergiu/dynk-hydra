import numpy as np


def cutout(inputs, patch_size=12):
    '''T. Devries and G. W. Taylor. Improved regularization of convolutional neural networks with cutout.
    http://arxiv.org/abs/1708.04552.'''

    # ***************************************************************************************************
    # Definition of cutout to use random pixel values for CIFAR10 and CIFAR100 as in https://arxiv.org/abs/1802.08530
    # M. D. McDonnell, Training wide residual networks for deployment using a single bit for each weight
    # ICLR, 2018
    # ***************************************************************************************************

    img_h, img_w, img_c = inputs.shape
    Loc1 = np.random.randint(2 - patch_size, img_h + 1)
    Loc2 = np.random.randint(2 - patch_size, img_w + 1)
    top = np.maximum(0, Loc1 - 1)
    bottom = np.minimum(img_h - 1, Loc1 + patch_size - 1 - 1)
    left = np.maximum(0, Loc2 - 1)
    right = np.minimum(img_w - 1, Loc2 + patch_size - 1 - 1)

    minn = np.min(inputs)
    maxx = np.max(inputs)

    new_values = minn + (np.random.randint(0, 256, (bottom - top + 1, right - left + 1, img_c)) / 255.0) * (maxx - minn)
    # new_values2 = minn + new_values * (maxx - minn)
    inputs[top:bottom + 1, left:right + 1, :] = new_values

    return inputs

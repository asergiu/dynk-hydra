import tensorflow as tf
import tensorflow.keras as keras


class KL_knowledge_distilation(keras.losses.Loss):
    def __init__(self, student_output, teacher_output, softmax_temperature=1, name="KL"):
        super().__init__(name=name)
        self.student_output = student_output
        self.teacher_output = teacher_output
        self.softmax_temperature = softmax_temperature

    def call(self, y_true, y_pred):
        student_with_softmax = keras.backend.softmax(self.student_output * self.softmax_temperature)
        teacher_with_softmax = keras.backend.softmax(self.teacher_output)
        return keras.losses.KLDivergence()(teacher_with_softmax, student_with_softmax)


class cce_and_weight_loss(keras.losses.Loss):
    def __init__(self, seeds, from_logits_students, from_logits_ensemble, name="cce_and_w_loss", w_w_loss=0.8):
        super(cce_and_weight_loss, self).__init__(name=name)
        self.w_w_loss = w_w_loss
        self.cce = _sparse_categorical_crossentropy_multi_output_and_weights(from_logits_students=from_logits_students,
                                                                             from_logits_ensemble=from_logits_ensemble,
                                                                             ensemble_loss_weight=len(seeds))
        self.loss_correct_weight = correct_prediction_weight_loss()

    def call(self, y_true, y_pred):
        # print("cce",self.cce(y_true, y_pred))
        # print("w",self.loss_correct_weight(y_true, y_pred))
        return self.cce(y_true, y_pred) + self.w_w_loss * self.loss_correct_weight(y_true, y_pred)


class cce_and_weight_loss_kd(keras.losses.Loss):
    def __init__(self, seeds, from_logits_students, from_logits_ensemble, softmax_temp, name="cce_and_w_loss", w_w_loss=0.8, w_kd=10):
        super(cce_and_weight_loss_kd, self).__init__(name=name)
        self.w_w_loss = w_w_loss
        self.cce = _sparse_categorical_crossentropy_multi_output_and_weights(from_logits_students=from_logits_students,
                                                                             from_logits_ensemble=from_logits_ensemble,
                                                                             ensemble_loss_weight=len(seeds))

        self.loss_correct_weight = correct_prediction_weight_loss()
        self.w_kd = w_kd
        if softmax_temp is not None:
            self.softmax_temp = softmax_temp
        else:
            self.softmax_temp = 1.0

    def call(self, y_true, y_pred):
        loss_kd = 0
        for i in range(len(y_pred) - 1):
            loss_kd += keras.losses.KLDivergence()(keras.backend.softmax(y_pred[0][-1] * self.softmax_temp), keras.backend.softmax(y_pred[0][i]))

        return self.cce(y_true, y_pred) + self.w_w_loss * self.loss_correct_weight(y_true, y_pred) + loss_kd * self.w_kd


class correct_prediction_weight_loss(keras.losses.Loss):
    def __init__(self, name="weights_loss"):
        super().__init__(name=name)
        self.w_gt = [0, 0]

    def call(self, y_true, y_pred):
        # input: y_pred is [stud_out, stud_out, stud_out, mean_out], [w1, w2, w3]
        weights = y_pred[1]

        # y_pred_classification = y_pred[0]

        # print("weights.shape", weights.shape)
        y_pred_students = [out for out in y_pred[0][:-1]]
        # print("len y_pred_students", len(y_pred_students))
        # print("y_pred_students[0].shape", y_pred_students[0].shape)
        y_pred_stack = keras.backend.stack(y_pred_students, axis=1)  # (bs, 3, 10)
        # print("y_pred_students.shape", y_pred_students.shape)

        y_true_one_hot = tf.one_hot(y_true, y_pred[0][1].shape[-1])  # (bs , 1, 10)

        max_pred = keras.backend.max(y_pred_stack, axis=-1)  # shape (sb, 3)
        max_pred_expand = tf.expand_dims(max_pred, axis=-1)  # (bs, 3, 1)

        pred_subtract_max = tf.subtract(y_pred_stack, max_pred_expand)  # (bs, 3, 10)
        # before_subtract_max = tf.reduce_sum(y_true_one_hot * y_pred_stack, axis=-1)
        y_true_one_hot = tf.expand_dims(y_true_one_hot, axis=1)
        # print("y_true_one_hot.shape", y_true_one_hot.shape)
        # print("pred_subtract_max.shape", pred_subtract_max.shape)

        gt_logits_isolated = y_true_one_hot * pred_subtract_max
        # print("y_true_one_hot[0]", y_true_one_hot[0])
        # print("pred_subtract_max[0][0]", pred_subtract_max[0][0])
        # print("gt_logits_isolated[0][0]", gt_logits_isolated[0][0])
        correct_pred_score = tf.reduce_sum(gt_logits_isolated, axis=-1)  # (bs, 3)

        correct_pred_score_plus_eps = correct_pred_score + keras.backend.epsilon()  # (bs, 3)
        correct_pred_score_plus_eps_relu = keras.backend.relu(correct_pred_score_plus_eps)  # (bs, 3)
        weights_gt = (correct_pred_score_plus_eps_relu * 1) / keras.backend.epsilon()  # (bs, 3)

        loss_weight = keras.losses.binary_crossentropy(weights_gt, weights)

        # loss_weight_reduce_sum = tf.reduce_sum(loss_weight)
        loss_weight_reduce_mean = tf.reduce_mean(loss_weight)
        return loss_weight_reduce_mean


class _sparse_categorical_crossentropy_multi_output_and_weights(keras.losses.Loss):

    def __init__(self, from_logits_students, ensemble_loss_weight, name="sparse_categorical_crossentropy_multi_output",
                 from_logits_ensemble=False, eval_only_ensemble_out=False):
        super().__init__(name=name)
        self.cce = sparse_categorical_crossentropy_multi_output(from_logits_students, ensemble_loss_weight, name, from_logits_ensemble, eval_only_ensemble_out)

    def call(self, y_true, y_pred):
        y_pred_no_w = y_pred[0]
        return self.cce(y_true, y_pred_no_w)


class _sparse_categorical_crossentropy(keras.losses.Loss):
    def __init__(self, from_logits, name="sparse_categorical_crossentropy"):
        super().__init__(name=name)
        self.from_logits = from_logits

    def call(self, y_true, y_pred):
        return keras.backend.mean(tf.keras.losses.sparse_categorical_crossentropy(y_true, y_pred, from_logits=self.from_logits))


class sparse_categorical_crossentropy_multi_output(keras.losses.Loss):
    # """
    # Computes the loss (BCE) of the individual models as well as the loss of the overall ensemble
    # """
    def __init__(self, from_logits_students, ensemble_loss_weight, name="sparse_categorical_crossentropy_multi_output", from_logits_ensemble=True, eval_only_ensemble_out=False):
        super().__init__(name=name)
        self.eval_only_ensemble_out = eval_only_ensemble_out
        self.from_logits_students = from_logits_students
        self.ensemble_loss_weight = ensemble_loss_weight
        self.from_logits_ensemble = from_logits_ensemble

    def call(self, y_true, y_pred):
        losses = []
        if self.eval_only_ensemble_out is False:
            for i in range(len(y_pred) - 1):
                losses.append(tf.reduce_mean(keras.backend.sparse_categorical_crossentropy(y_true, y_pred[i], from_logits=self.from_logits_students)))

        losses.append(self.ensemble_loss_weight * tf.reduce_mean(
            keras.backend.sparse_categorical_crossentropy(y_true, y_pred[len(y_pred) - 1], from_logits=self.from_logits_ensemble)))

        loss_sum = tf.reduce_sum(losses)
        return loss_sum


class _categorical_crossentropy(keras.losses.Loss):
    def __init__(self, from_logits, name="_categorical_crossentropy"):
        super().__init__(name=name)
        self.from_logits = from_logits
        self.cc = keras.losses.CategoricalCrossentropy()

    def call(self, y_true, y_pred):
        return self.cc(y_true, y_pred)


class feature_similarity_loss(keras.losses.Loss):
    # """
    # Computes the loss define by the feature similarity: the features of the network should be as
    # Input: comparable_layers_dict should be something like:
    # {
    #   1: [var1, var2, ..., var_n],
    #   2: [var1, var2, ..., var_n]
    #   ...
    #   m: [var1, var2, ..., var_n]
    # }, where n is the number of students, and m is the number of variables we want to compare
    #
    # """
    def __init__(self, comparable_layers_dict, distance_type, name="similarity_loss"):
        super().__init__(name=distance_type + name)

        self.comparable_layers_dict = comparable_layers_dict
        self.distance_type = distance_type

    def cosine_distance(self, v1, v2):
        len_vector1 = tf.sqrt(tf.reduce_sum(keras.backend.square(v1)))
        len_vector2 = tf.sqrt(tf.reduce_sum(keras.backend.square(v2)))
        cos_dist = keras.backend.sum(v1 * v2) / (tf.multiply(len_vector1, len_vector2) + keras.backend.epsilon())  # -0.29 / 58

        # dist = keras.backend.maximum(tf.Variable(0, dtype=tf.float32), cos_dist)
        dist = tf.abs(cos_dist)
        # dist = cos_dist
        return dist

    def normalized_euclidian_distance(self, v1, v2):
        numerator = keras.backend.var(v1 - v2)
        denominator = keras.backend.var(v1) + keras.backend.var(v2)
        norm_distance = numerator / (2.0 * denominator)

        return 1.0 - norm_distance

    def call(self, y_true, y_pred):

        distances = []
        keys = self.comparable_layers_dict.keys()

        dist_func = self.normalized_euclidian_distance
        if self.distance_type == 'cosine_dist':
            dist_func = self.cosine_distance

        for key in keys:
            layers = self.comparable_layers_dict[key]
            # if len(layers) != 3:
            #     raise NotImplementedError('Not implemented yet!')

            distances.append(dist_func(layers[0], layers[1]))
            distances.append(dist_func(layers[0], layers[2]))
            distances.append(dist_func(layers[1], layers[2]))

        return keras.backend.sum(distances)

<h2> Training scripts </h2>

- **Train HydraRes or DynK-Hydra using:**

`dynk-hydra/train_scripts/train_hydra_or_dynk.py`

- **Train Resent:**

`dynk-hydra/train_scripts/train_resnet.py`

<h2> Conditional computation backpropagation flow </h2>

  ![backprop flow](/assets/backprop.png)

<h2> Results </h2>

  ![numerical results](/assets/numerical_results.PNG)

<h2> Update (results on Imagenet (32x32) and CUB-200 (224x224)) </h2>

  ![numerical results](/assets/imgnet-cub.png)

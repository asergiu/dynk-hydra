import tensorflow as tf

# tf.config.experimental_run_functions_eagerly(True)
from tensorflow import keras
import tensorflow as tf


class individual_scc_loss(keras.losses.Loss):
    def __init__(self, output_layers, name="individual_scc_loss"):
        super().__init__(name=name)
        self.output_layers = output_layers

    def call(self, y_true, y_pred):
        individual_scc_list = []

        for out_layer in self.output_layers:
            individual_y_pred = out_layer
            # individual_scc_list.append(keras.backend.sparse_categorical_crossentropy(y_true, individual_y_pred, from_logits=True, axis=-1))
        # mean_scc = keras.backend.mean(individual_scc_list)
        # mean_scc = individual_scc_list[0]
        # y_pred = ops.convert_to_tensor_v2(y_pred)
        # y_true = math_ops.cast(y_true, y_pred.dtype)
        individual_scc_list.append(keras.backend.sparse_categorical_crossentropy(y_true, y_pred, from_logits=True, axis=-1))
        mean_scc = keras.backend.sum(individual_scc_list, axis=0)
        return mean_scc


class weights_cosine_loss(keras.losses.Loss):
    def __init__(self, layers, name="weights_cosine_loss"):
        super().__init__(name=name)
        self.layers = layers

    def call(self, y_true, y_pred):
        # return cos_distance_v1(self.layers)
        return cos_distance(self.layers)


def correct_prediction_weight_loss_fn(y_true, y_pred, weights):
    y_true_one_hot = tf.one_hot(y_true, y_pred.shape[-1])
    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ', weights)
    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ y_pred', y_pred.shape)

    correct_pred_score = tf.reduce_sum(y_true_one_hot * y_pred, axis=-1)  # None, 3
    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ y_true', y_true_one_hot.shape)
    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ correct_pred_score', correct_pred_score.shape)
    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ weights: ', weights.shape)
    return keras.losses.binary_crossentropy(weights, correct_pred_score)


class correct_prediction_weight_loss(keras.losses.Loss):

    def __init__(self, weights, name="correct_prediction_weight"):
        super().__init__(name=name)
        self.weights = weights

    def call(self, y_true, y_pred):
        y_true_one_hot = tf.one_hot(y_true, y_pred.shape[-1])
        print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ', self.weights)
        # print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ y_pred', y_pred.shape)

        correct_pred_score = tf.reduce_sum(y_true_one_hot * y_pred, axis=-1)  # None, 3
        # print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ y_true', y_true_one_hot.shape)
        # print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ correct_pred_score', correct_pred_score.shape)
        # print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ weights: ', self.weights.shape)
        weights = keras.backend.constant(self.weights, shape=self.weights.shape)
        return keras.losses.binary_crossentropy(weights, correct_pred_score)


def cos_distance(layers):
    layers_flt = []
    # print('layers flt: ', len(layers))
    for model_vars in layers:
        # print ('layer shape: ', model_vars.shape)
        all_vars_flt = []
        for var in model_vars:
            all_vars_flt.append(keras.backend.flatten(var))
        all_vars_flt_concat = keras.backend.concatenate(all_vars_flt)
        # print('all vars concat shape: ', all_vars_flt_concat.shape)
        layers_flt.append(all_vars_flt_concat)

    len1 = keras.backend.sqrt(keras.backend.sum(keras.backend.square(layers_flt[0])))
    len2 = keras.backend.sqrt(keras.backend.sum(keras.backend.square(layers_flt[1])))
    len3 = keras.backend.sqrt(keras.backend.sum(keras.backend.square(layers_flt[2])))
    cos1 = keras.backend.abs(keras.backend.sum(layers_flt[0] * layers_flt[1]) / (len1 * len2))
    cos2 = keras.backend.abs(keras.backend.sum(layers_flt[0] * layers_flt[2]) / (len1 * len3))
    cos3 = keras.backend.abs(keras.backend.sum(layers_flt[1] * layers_flt[2]) / (len2 * len3))

    sum = keras.backend.sum([cos1, cos2, cos3])
    return sum


# def cos_distance_v1(layers):
#     # layers_flt = []
#     layers_flt = [keras.backend.flatten(layer) for layer in layers]
#     len1 = keras.backend.sqrt(keras.backend.sum(keras.backend.square(layers_flt[0])))
#     len2 = keras.backend.sqrt(keras.backend.sum(keras.backend.square(layers_flt[1])))
#     len3 = keras.backend.sqrt(keras.backend.sum(keras.backend.square(layers_flt[2])))
#     cos1 = keras.backend.abs(keras.backend.sum(layers_flt[0] * layers_flt[1]) / (len1 * len2))
#     cos2 = keras.backend.abs(keras.backend.sum(layers_flt[0] * layers_flt[2]) / (len1 * len3))
#     cos3 = keras.backend.abs(keras.backend.sum(layers_flt[1] * layers_flt[2]) / (len2 * len3))
#     sum = keras.backend.sum([cos1, cos2, cos3])
#     return sum


def weights_eucl_loss(layers):
    # Create a loss function that adds the MSE loss to the mean of all squared activations of a specific layer

    def loss(y_true, y_pred):
        layers_flt = [keras.backend.flatten(layer) for layer in layers]

        eucl = -keras.backend.sqrt(keras.backend.sum(keras.backend.square(layers_flt[0] - layers_flt[1]), axis=-1))
        eucl2 = -keras.backend.sqrt(keras.backend.sum(keras.backend.square(layers_flt[0] - layers_flt[2]), axis=-1))
        eucl3 = -keras.backend.sqrt(keras.backend.sum(keras.backend.square(layers_flt[1] - layers_flt[2]), axis=-1))

        return keras.backend.sum([eucl, eucl2, eucl3])

    return loss


class CustomLearningRateScheduler(keras.callbacks.Callback):

    def __init__(self, schedule):
        super(CustomLearningRateScheduler, self).__init__()
        self.schedule = schedule

    def on_epoch_begin(self, epoch, logs=None):
        lr = float(keras.backend.get_value(self.model.optimizer.learning_rate))
        scheduled_lr = self.schedule(epoch, lr)
        keras.backend.set_value(self.model.optimizer.lr, scheduled_lr)
        print("\nEpoch %05d: Learning rate is %6.4f." % (epoch, scheduled_lr))


LR_SCHEDULE = [
    (3, 0.05),
    (6, 0.01),
    (9, 0.005),
    (12, 0.001),
]


def lr_schedule(epoch, lr):
    if epoch < LR_SCHEDULE[0][0] or epoch > LR_SCHEDULE[-1][0]:
        return lr
    for i in range(len(LR_SCHEDULE)):
        if epoch == LR_SCHEDULE[i][0]:
            return LR_SCHEDULE[i][1]
    return lr
# usage lr decay callbacks=[CustomLearningRateScheduler(lr_schedule), ..]

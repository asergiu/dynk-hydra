import glob
import os

import numpy as np
from tensorflow import keras
from tensorflow.keras.datasets import cifar10, cifar100

from tools.ds_playground import unpickle
from tools.utils import get_image_cluster_indices


def parse_cifar(ds_name, to_categorical=False, num_classes=-1, coarse_label=False, subtract_pixel_mean=True,
                cluster_path=None):
    ds = cifar10.load_data() if ds_name == "cifar10" else cifar100.load_data()
    (x_train, y_train), (x_test, y_test) = ds
    from tensorflow.keras.applications.resnet50 import preprocess_input

    y_train = y_train.astype('int32')
    y_test = y_test.astype('int32')

    if subtract_pixel_mean:
        x_train = x_train.astype('float32') / 255
        x_test = x_test.astype('float32') / 255
        x_train_mean = np.mean(x_train, axis=0)
        x_train -= x_train_mean
        x_test -= x_train_mean
    else:
        x_train = preprocess_input(x_train)
        x_test = preprocess_input(x_test)

    if coarse_label:
        cluster_indices = get_image_cluster_indices(cluster_path=cluster_path)
        y_train = cluster_indices[:50000]
        y_test = cluster_indices[50000:]
        num_classes = len(np.unique(y_train))

    if to_categorical:
        y_train = keras.utils.to_categorical(y_train, num_classes)
        y_test = keras.utils.to_categorical(y_test, num_classes)

    return (x_train, y_train), (x_test, y_test)


def _parse_imagenet_32(data_file, img_size=32, mean=None):
    d = unpickle(data_file)
    x = d['data']
    y = d['labels']
    if mean is None:
        mean_image = d['mean']
    else:
        mean_image = mean

    x = x / np.float32(255)
    mean_image = mean_image / np.float32(255)

    y = [i - 1 for i in y]
    data_size = x.shape[0]
    x -= mean_image
    img_size2 = img_size * img_size
    x = np.dstack((x[:, :img_size2], x[:, img_size2:2 * img_size2], x[:, 2 * img_size2:]))
    x = x.reshape((x.shape[0], img_size, img_size, 3))
    X_train = x[0:data_size, :, :, :]
    Y_train = y[0:data_size]
    if mean is None:
        return X_train, Y_train, mean_image
    return X_train, Y_train


def _parse_imagenet_32_raw(data_file, img_size=32):
    d = unpickle(data_file)
    x = d['data']
    y = d['labels']

    y = [i - 1 for i in y]
    data_size = x.shape[0]

    img_size2 = img_size * img_size
    x = np.dstack((x[:, :img_size2], x[:, img_size2:2 * img_size2], x[:, 2 * img_size2:]))
    x = x.reshape((x.shape[0], img_size, img_size, 3))
    X_train = x[0:data_size, :, :, :]
    Y_train = y[0:data_size]

    return X_train, Y_train


def parse_food101(img_size=224, custom_preprocess=None, to_categorical=True, for_test_only=False, data_folder=""):

    x_train_mean = np.array([0.21629043, 0.21629043, 0.21629043])

    y_test_path = os.path.join(data_folder, "y_test_%d.npy" % img_size)
    y_train_path = os.path.join(data_folder, "y_train_%d.npy" % img_size)
    x_test_path = os.path.join(data_folder, "x_test_%d.npy" % img_size)

    if not os.path.exists(y_test_path):
        print("Write the npy files for future reading optimizations ...")
        classes = sorted([dir_name for dir_name in os.listdir(data_folder) if os.path.isdir(os.path.join(data_folder,dir_name))])
        print("classes", classes)
        from cv2 import cv2
        split_train_percent = 0.75
        x_train = []
        y_train = []
        x_test = []
        y_test = []
        batch_limit = int(len(classes) / 7)
        b = 0
        for cl_idx, cl in enumerate(classes):
            files = np.array(sorted(glob.glob(os.path.join(data_folder, cl, "**/*.jpg"), recursive=True)))
            split_train = int(len(files) * split_train_percent)
            for in_file in files[:split_train]:
                x_train.append(cv2.imread(in_file))
                y_train.append(cl_idx)
            for in_file in files[split_train:]:
                x_test.append(cv2.imread(in_file))
                y_test.append(cl_idx)

            if (cl_idx % batch_limit == 0 and cl_idx != 0) or cl_idx == len(classes) - 1:
                x_train = np.array(x_train)
                np.save(os.path.join(data_folder, "x_train_%d_b%d.npy" % (img_size, b)), x_train)
                x_train = []
                b += 1
                print("write batch", b)
            print("process class", cl_idx)
        y_train = np.array(y_train)
        x_test = np.array(x_test)
        y_test = np.array(y_test)

        np.save(y_train_path, y_train)
        np.save(x_test_path, x_test)
        np.save(y_test_path, y_test)
        print("done writing npys")
    else:
        print("npy found, read them from ", data_folder)

    x_train = None
    # if True or not for_test_only:
    if not for_test_only:
        train_npys = sorted(glob.glob(os.path.join(data_folder, "x_train*.npy"), recursive=True))
        for tr_npy in train_npys:
            current_x_train = np.load(tr_npy)
            if x_train is None:
                x_train = current_x_train
            else:
                x_train = np.concatenate((x_train, current_x_train))

        x_test = np.load(x_test_path)
        y_train = np.load(y_train_path)
        y_test = np.load(y_test_path)

    if custom_preprocess:
        x_test = custom_preprocess(x_test)
        if x_train is not None:
            x_train = custom_preprocess(x_train)
    else:
        if x_train is not None:
            x_train = x_train.astype('float32') / 255
            x_train_mean = np.mean(x_train, axis=0)
            x_train -= x_train_mean

        x_test = x_test.astype('float32') / 255
        x_test -= x_train_mean
        # x_train_mean = np.mean(x_train, axis=tuple(range(x_train.ndim-1)))

    if to_categorical:
        num_classes = len(np.unique(y_train))
        y_train = keras.utils.to_categorical(y_train, num_classes)
        y_test = keras.utils.to_categorical(y_test, num_classes)

    return (x_train, y_train), (x_test, y_test)


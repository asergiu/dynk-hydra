import tensorflow as tf
from tensorflow.keras.layers import Input

from tools.resnet20_keras_example import resSep_layer, resnet_layer


def resnetStem_tf1_unaltered(input_shape, depth, num_stem_layers=2, do_wd_schedule=False):
    if (depth - 2) % 6 != 0:
        raise ValueError('depth should be 6n+2 (eg 20, 32, 44 in [a])')

    num_filters = 16
    num_res_blocks = int((depth - 2) / 6)

    inputs = Input(shape=input_shape)
    x = tf.keras.layers.Conv2D(num_filters, 3)(inputs)

    for stack in range(num_stem_layers):
        for res_block in range(num_res_blocks):
            strides = 1
            if stack > 0 and res_block == 0:  # first layer but not first stack
                strides = 2
            y = resnet_layer(inputs=x,
                             num_filters=num_filters,
                             strides=strides, add_l2_w=(not do_wd_schedule))
            y = resnet_layer(inputs=y,
                             num_filters=num_filters,
                             activation=None, add_l2_w=(not do_wd_schedule))
            # if depth >= 44:
            #     y = resnet_layer(inputs=y, kernel_size=1,
            #                      num_filters=num_filters,
            #                      activation=None, add_l2_w=(not do_wd_schedule))
            if stack > 0 and res_block == 0:
                x = resnet_layer(inputs=x,
                                 num_filters=num_filters,
                                 kernel_size=1,
                                 strides=strides,
                                 activation=None,
                                 batch_normalization=False, add_l2_w=(not do_wd_schedule))
            x = tf.keras.layers.add([x, y])
            x = tf.keras.layers.Activation('relu')(x)
        num_filters *= 2

    model = tf.keras.Model(inputs=inputs, outputs=x)
    return model, num_filters


def resnetAfterStem_tf1_unaltered(input_shape, depth, num_filters, num_stem_layers=3, total_nr_blocks=4, num_classes=0,
                                  activation=None,
                                  return_classification_only=False, do_wd_schedule=False, dropout_rate=0):
    num_res_blocks = int((depth - 2) / 6)
    inputs = Input(shape=input_shape)
    x = inputs
    stack_layers = [i for i in range(num_stem_layers, total_nr_blocks)]
    for _ in stack_layers:
        for res_block in range(num_res_blocks):
            strides = 1
            if res_block == 0:
                strides = 2
            y = resnet_layer(inputs=x,
                             num_filters=num_filters,
                             strides=strides, add_l2_w=(not do_wd_schedule))
            y = resnet_layer(inputs=y,
                             num_filters=num_filters,
                             activation=None, add_l2_w=(not do_wd_schedule))
            # if depth >= 44:
            #     y = resnet_layer(inputs=y, kernel_size=1,
            #                      num_filters=num_filters,
            #                      activation=None, add_l2_w=(not do_wd_schedule))
            if res_block == 0:
                x = resnet_layer(inputs=x,
                                 num_filters=num_filters,
                                 kernel_size=1,
                                 strides=strides,
                                 activation=None,
                                 batch_normalization=False, add_l2_w=(not do_wd_schedule))
            x = tf.keras.layers.add([x, y])
            x = tf.keras.layers.Activation('relu')(x)

    projection = x
    output_list = [projection]

    if num_classes > 0:
        projection_gap = tf.keras.layers.GlobalAveragePooling2D()(projection)
        y = tf.keras.layers.Flatten()(projection_gap)
        if dropout_rate >= 0.01:
            y = tf.keras.layers.Dropout(dropout_rate)(y)
        classification_layer = tf.keras.layers.Dense(num_classes,
                                                     activation=activation,
                                                     kernel_initializer='he_normal')(y)
        if return_classification_only:
            model = tf.keras.Model(inputs=inputs, outputs=classification_layer)
            return model

        output_list.append(classification_layer)

    model = tf.keras.Model(inputs=inputs, outputs=output_list)

    return model

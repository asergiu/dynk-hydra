import itertools
import os
import sys
from abc import ABC

import phate
import sklearn

sys.path.append(os.path.join(os.getcwd(), '.'))
sys.path.append(os.path.join(os.getcwd(), '..'))
sys.path.append(os.path.join(os.getcwd(), '../..'))
sys.path.append(os.path.join(os.getcwd(), '../../..'))

from tools.de_utils import parse_food101
from tools.utils import get_image_cluster_indices, make_sure_dir_exists
from tools.model_utils import model_version
import numpy as np
from keras_preprocessing.image import ImageDataGenerator
from sklearn.cluster import KMeans
from tensorflow.keras.datasets import cifar100
import tensorflow.keras as keras
import tensorflow as tf
import efficientnet.tfkeras as efn
import matplotlib.pyplot as plt
import collections
from scipy.spatial import distance
from sklearn.metrics import accuracy_score
import sklearn.metrics as metrics

config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.compat.v1.Session(config=config)

root_working_dir = "subtask_partitioning"
make_sure_dir_exists(root_working_dir)
kmeans_dir = os.path.join(root_working_dir, "kmeans")
agglomerative_dir = os.path.join(root_working_dir, "agglomerative")
make_sure_dir_exists(kmeans_dir)
make_sure_dir_exists(agglomerative_dir)


class EmbeddingNet(keras.Model, ABC):
    def __init__(self, model_v, weights='imagenet'):
        super(EmbeddingNet, self).__init__()
        self.img_size = 224
        self.resize = tf.keras.layers.experimental.preprocessing.Resizing(self.img_size, self.img_size,
                                                                          interpolation="bicubic")
        m = model_version(model_v)
        self.model = m(include_top=False, weights=weights, input_shape=(self.img_size, self.img_size, 3), pooling="avg")
        self.model.trainable = False
        self.projection_size = self.model.layers[-1].output.shape[1]

    @tf.function
    def call(self, data, training=None):
        data = self.resize(data)
        emb = self.model(data, training=False)
        return emb


def get_images_embeddings(model_v=0):
    bs = 100

    emb_path = "class_img_embedding_efficientB%d.npy" % model_v
    (X_train, y_train), (x_test, y_test) = cifar100.load_data()
    x = np.concatenate((X_train, x_test), axis=0)
    y = np.concatenate((y_train, y_test), axis=0)

    if not os.path.exists(emb_path):
        x = efn.preprocess_input(x)
        emb_model = EmbeddingNet(model_v=model_v)
        num_classes = len(np.unique(y_test))

        imgs_per_class = int(len(x) / num_classes)  # train plus test

        sample_embeddings = [[] for _ in range(num_classes)]
        for cls_idx in range(num_classes):
            class_idx_start = cls_idx * imgs_per_class
            class_idx_end = (cls_idx + 1) * imgs_per_class
            sample_X = x[class_idx_start: class_idx_end]
            sample_Y = y[class_idx_start: class_idx_end]
            steps_per_epoch = max(1, int(sample_Y.shape[0] / bs))
            for batch_idx, (image_batch, y_batch) in enumerate(
                    ImageDataGenerator().flow(sample_X, sample_Y, batch_size=bs, shuffle=False)):
                if batch_idx == steps_per_epoch:
                    break
                sample_embeddings[cls_idx].extend(emb_model.get_projection(image_batch, training=False).numpy())
                print("cls %d batch %d/%d" % (cls_idx, batch_idx, steps_per_epoch))

        sample_embeddings = np.asarray(sample_embeddings)
        np.save(emb_path, sample_embeddings)
    else:
        sample_embeddings = np.load(emb_path, allow_pickle=True)
        print("load embeddings from", emb_path)
    return sample_embeddings, y


def retrieve_info(cluster_labels, y_train):
    # Associates most probable label with each cluster in KMeans model
    # returns: dictionary of clusters assigned to each label
    reference_labels = {}
    # For loop to run through each label of cluster label
    for i in range(len(np.unique(cluster_labels))):
        index = np.where(cluster_labels == i, 1, 0)
        num = np.bincount(y_train[index == 1]).argmax()
        reference_labels[i] = num
    return reference_labels


def measure_cluster2(model_v, clusters_labels, y_train):
    reference_labels = retrieve_info(clusters_labels, y_train)
    number_labels = np.random.rand(len(clusters_labels))
    for i in range(len(clusters_labels)):
        number_labels[i] = reference_labels[clusters_labels[i]]

    homogeneity_score = metrics.homogeneity_score(y_train, clusters_labels)
    # inertia = kmeans.inertia_
    summary_file = os.path.join(kmeans_dir, "summary%d.txt" % model_v)
    print("homogeneity_score ", homogeneity_score)
    # print("inertia ", inertia)
    with open(summary_file, "a") as w_file:
        w_file.write("\nhomogeneity_score %.4f" % homogeneity_score)
        # w_file.write("\ninertia %.4f" % inertia)


def float_array_to_str(arr):
    return ", ".join(["%.2f" % itm for itm in arr])


def distance_between_emb_matrix(emb1, emb2):
    distances = np.triu(sklearn.metrics.pairwise_distances(emb1, emb2))
    distances_idx = np.where(distances > 0)
    return distances[distances_idx]


def measure_cluster(model_v, embeddings_flatten, n_clusters, clusters_labels, cluster_centers):
    min_inter_clusters_distances = []
    centroids_distance = []

    inter_cluster_distances_means = []
    inter_cluster_distances_stds = []
    intra_cluster_distances_means = []
    intra_cluster_distances_stds = []
    for cluster in range(n_clusters):
        idx_cluster = np.where(clusters_labels == cluster)[0]
        embds_cluster = embeddings_flatten[idx_cluster]

        intra_distance_in_cluster = distance_between_emb_matrix(embds_cluster, embds_cluster)
        intra_dist_mean = np.mean(intra_distance_in_cluster)
        intra_dist_std = np.std(intra_distance_in_cluster)

        intra_cluster_distances_means.append(intra_dist_mean)
        intra_cluster_distances_stds.append(intra_dist_std)

    print("calculate min inter clusters distance ....")
    for clusters_tuple in itertools.combinations(range(n_clusters), 2):
        idx_cluster_0 = np.where(clusters_labels == clusters_tuple[0])[0]
        idx_cluster_1 = np.where(clusters_labels == clusters_tuple[1])[0]
        embds_cluster_0 = embeddings_flatten[idx_cluster_0]
        embds_cluster_1 = embeddings_flatten[idx_cluster_1]
        inter_distance_in_cluster = distance_between_emb_matrix(embds_cluster_0, embds_cluster_1)
        inter_dist_mean = np.mean(inter_distance_in_cluster)
        inter_dist_std = np.std(inter_distance_in_cluster)

        inter_cluster_distances_means.append(inter_dist_mean)
        inter_cluster_distances_stds.append(inter_dist_std)

        min_distance_list = sklearn.metrics.pairwise_distances_argmin_min(embds_cluster_0, embds_cluster_1)
        min_inter_distance = min_distance_list[1].min()
        min_inter_clusters_distances.append(min_inter_distance)
        if len(cluster_centers) > 0:
            centroids_distance.append(
                distance.euclidean(cluster_centers[clusters_tuple[0]], cluster_centers[clusters_tuple[1]]))

    min_inter_clusters_distances = np.asarray(min_inter_clusters_distances)

    cluster_count = []
    for cluster in range(n_clusters):
        cluster_indices = np.where(clusters_labels == cluster)[0]
        cluster_count.append(len(cluster_indices))

    print("calculate intra clusters distances ....")
    intra_cluster_distances_mean = [[] for _ in range(n_clusters)]
    intra_cluster_distances_std = [[] for _ in range(n_clusters)]
    for cl in range(n_clusters):
        idx_cluster = np.where(clusters_labels == cl)[0]
        embds_cluster = embeddings_flatten[idx_cluster]
        if len(cluster_centers) > 0:
            distances_to_centroid = sklearn.metrics.pairwise_distances([cluster_centers[cl]], embds_cluster)
            intra_cluster_distances_mean[cl] = distances_to_centroid[0].mean()
            intra_cluster_distances_std[cl] = distances_to_centroid[0].std()

    if len(cluster_centers) > 0:
        intra_cluster_distances_mean = np.asarray(intra_cluster_distances_mean)

    if len(cluster_centers) > 0:
        centroids_distance = np.asarray(centroids_distance)
        compactness = intra_cluster_distances_mean.mean()
        separation_between_centers = centroids_distance.mean()
    separation_min_dist = min_inter_clusters_distances.mean()

    cluster_count_str = ", ".join(["%d" % itm for itm in cluster_count])
    if len(cluster_centers) > 0:
        intra_cluster_distance_std_str = ", ".join(["%.3f" % itm for itm in intra_cluster_distances_std])

    summary_file = os.path.join(kmeans_dir, "summary%d.txt" % model_v)

    inter_cluster_distances_means = np.asarray(inter_cluster_distances_means)
    inter_cluster_distances_stds = np.asarray(inter_cluster_distances_stds)
    intra_cluster_distances_means = np.asarray(intra_cluster_distances_means)
    intra_cluster_distances_stds = np.asarray(intra_cluster_distances_stds)

    inter_cluster_distances_means_str = float_array_to_str(inter_cluster_distances_means)
    inter_cluster_distances_stds_str = float_array_to_str(inter_cluster_distances_stds)
    intra_cluster_distances_means_str = float_array_to_str(intra_cluster_distances_means)
    intra_cluster_distances_stds_str = float_array_to_str(intra_cluster_distances_stds)

    with open(summary_file, "a") as w_file:
        w_file.write("\nnr clusters %d" % n_clusters)
        w_file.write("\ncluster count %s" % cluster_count_str)
        w_file.write("\nmin inter_clusters distances mean %.1f" % min_inter_clusters_distances.mean())
        w_file.write("\n mean intra cluster distances %s with mean %.2f" % (
            intra_cluster_distances_means_str, intra_cluster_distances_means.mean()))
        w_file.write("\n std intra cluster distances %s with mean %.2f" % (
            intra_cluster_distances_stds_str, intra_cluster_distances_stds.mean()))
        w_file.write("\n mean inter cluster distances %s with mean %.2f" % (
            inter_cluster_distances_means_str, inter_cluster_distances_means.mean()))
        w_file.write("\n std inter cluster distances %s with mean %.2f" % (
            inter_cluster_distances_stds_str, inter_cluster_distances_stds.mean()))
        if len(cluster_centers) > 0:
            w_file.write(
                "\nintra cluster distances to centroid std_dev: %s" % intra_cluster_distance_std_str + "\n   mean %.1f" % (
                    intra_cluster_distances_mean.mean()))
            w_file.write("\ncentroids pairs distance mean %.1f" % centroids_distance.mean())
            w_file.write("\ncentroids_pairs_distance/compactness: %.4f" % (separation_between_centers / compactness))
            w_file.write("\nmin_inter_distance/compactness: %.4f" % (separation_min_dist / compactness))
        w_file.write("\n")
    print("")


def split_unbalanced_classes():
    model_v = 0
    # embeddings, ys = get_images_embeddings(model_v=model_v)
    for n_clusters in [5, 10, 20]:
        # embeddings = embeddings[:, :5, :]
        # imgs_per_class = embeddings.shape[1]
        #
        # gt_path = os.path.join(kmeans_dir, "gt%d.npy" % model_v)
        #
        kmeans_labels_path = os.path.join(kmeans_dir, "kmeans_labels_model_v%d_studs%d.npy" % (model_v, n_clusters))
        # embeddings_flatten = np.reshape(embeddings, (embeddings.shape[0] * embeddings.shape[1], embeddings.shape[2]))

        kmeans_centers_path = os.path.join(kmeans_dir, "kmeans_center_model_v%d_studs%d.npy" % (model_v, n_clusters))
        if not os.path.exists(kmeans_labels_path) or not os.path.exists(kmeans_centers_path):
            print("run kmeans ...")
            kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(embeddings_flatten)
            clusters_labels = kmeans.labels_
            cluster_centers = kmeans.cluster_centers_
            np.save(kmeans_labels_path, clusters_labels)
            np.save(kmeans_centers_path, cluster_centers)
            print("end kmeans")
        else:
            clusters_labels = np.load(kmeans_labels_path, allow_pickle=True)
            cluster_centers = np.load(kmeans_centers_path, allow_pickle=True)
            print("load kmeans labels from:", kmeans_labels_path)
            print("load kmeans results from:", kmeans_centers_path)

        imgs_per_class = 600
        num_classes = 100

        ds = cifar100.load_data()
        (_, y_train), (_, y_test) = ds
        y_flatten = np.squeeze(np.concatenate((y_train, y_test), axis=0))

        class_to_cluster_bin = []
        class_to_best_cluster = []

        for cls in range(num_classes):
            cls_idx = np.where(y_flatten == cls)
            class_to_cluster_bin.append(np.bincount(clusters_labels[cls_idx]))
            class_to_best_cluster.append(np.argmax(class_to_cluster_bin[-1]))
        class_to_best_cluster = np.array(class_to_best_cluster)
        cluster_statistics = np.bincount(class_to_best_cluster)
        print(cluster_statistics)


def split_unbalanced_images():
    model_v = 0
    embeddings, ys = get_images_embeddings(model_v=model_v)
    for n_clusters in [10]:
        # embeddings = embeddings[:, :5, :]
        imgs_per_class = embeddings.shape[1]

        kmeans_labels_path = os.path.join(kmeans_dir, "kmeans_labels_model_v%d_studs%d.npy" % (model_v, n_clusters))
        gt_path = os.path.join(kmeans_dir, "gt%d.npy" % model_v)

        embeddings_flatten = np.reshape(embeddings, (embeddings.shape[0] * embeddings.shape[1], embeddings.shape[2]))
        # if not os.path.exists(kmeans_labels_path):
        if True:
            y_flatten = []
            for cls_idx in range(embeddings.shape[0]):
                y_flatten.extend([cls_idx for _ in range(imgs_per_class)])
            y_flatten = np.asarray(y_flatten)

            kmeans_centers_path = os.path.join(kmeans_dir,
                                               "kmeans_center_model_v%d_studs%d.npy" % (model_v, n_clusters))
            if not os.path.exists(kmeans_labels_path) or not os.path.exists(kmeans_centers_path):
                print("run kmeans ...")
                kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(embeddings_flatten)
                clusters_labels = kmeans.labels_
                cluster_centers = kmeans.cluster_centers_
                np.save(gt_path, y_flatten)
                np.save(kmeans_labels_path, clusters_labels)
                np.save(kmeans_centers_path, cluster_centers)
                print("end kmeans")
            else:
                clusters_labels = np.load(kmeans_labels_path, allow_pickle=True)
                cluster_centers = np.load(kmeans_centers_path, allow_pickle=True)
                print("load kmeans labels from:", kmeans_labels_path)
                print("load kmeans results from:", kmeans_centers_path)

            tsv_file = os.path.join(kmeans_dir, "plots/cluster_image_embds_%d.tsv" % model_v)
            tsv_meta_file = os.path.join(kmeans_dir, "plots/meta_raw_class_%d.tsv" % model_v)

            ds = cifar100.load_data()
            (_, y_train), (_, y_test) = ds
            y_flatten = np.squeeze(np.concatenate((y_train, y_test), axis=0))

            # with open(tsv_file, "w") as tsv_writer:
            #     for emb in embeddings_flatten:
            #         tsv_writer.write("\t".join(["%.8f" % itm for itm in emb]))
            #         tsv_writer.write("\n")

            with open(tsv_meta_file, "w") as tsv_writer:
                for i in y_flatten:
                    tsv_writer.write(str(i))
                    tsv_writer.write("\n")
            exit()

            measure_cluster(model_v, embeddings_flatten, n_clusters, clusters_labels, cluster_centers)
            measure_cluster2(model_v, clusters_labels, ys)
        else:
            kmeans_labels = np.load(kmeans_labels_path, allow_pickle=True)
            y_flatten = np.load(gt_path, allow_pickle=True)
            print("load kmeans_labels from ", kmeans_labels_path)

        # print("run visualization")
        # visualize_clustering(embeddings_flatten, kmeans_labels, y_flatten, model_v)


def visualize_clustering(embeddings, clusters, gt_class, model_v):
    histogram_dir = kmeans_dir
    nr_clusters = len(np.unique(clusters))
    num_classes = len(np.unique(gt_class))
    cluster_hist = [{} for _ in range(nr_clusters)]
    for cluster in range(nr_clusters):
        for cls in range(num_classes):
            cluster_hist[cluster][cls] = 0

    image_to_cluster_mapping = {}
    for cls_idx in range(num_classes):
        image_to_cluster_mapping[cls_idx] = []

    for cluster in range(nr_clusters):
        in_cluster = gt_class[clusters == cluster]
        for cls in in_cluster:
            image_to_cluster_mapping[cls].append(cluster)
            cluster_hist[cluster][cls] += 1
    summary_file = os.path.join(histogram_dir, "summary.txt")
    with open(summary_file, "w") as w_file:
        for cls_idx in range(num_classes):
            w_file.write("cl %d: " % cls_idx)
            collection = collections.Counter(image_to_cluster_mapping[cls_idx])
            for item in np.unique(image_to_cluster_mapping[cls_idx]):
                w_file.write("{%d: %d} " % (item, collection[item]))
            w_file.write("\n")

    make_sure_dir_exists(histogram_dir)
    for cluster_idx in range(nr_clusters):
        images_in_cluster = np.fromiter(cluster_hist[cluster_idx].values(), dtype=int).sum()
        fig = plt.figure()
        ax = fig.add_subplot()
        ax.set_title("images_in_cluster %d" % images_in_cluster, fontsize=15)
        plt.bar(list(cluster_hist[cluster_idx].keys()), cluster_hist[cluster_idx].values(), color='g')
        plt.savefig(os.path.join(histogram_dir, "modelV%d_cluster_%d.png" % (model_v, cluster_idx)))
        plt.close()

    phate_operator = phate.PHATE(knn=nr_clusters, n_jobs=-1, n_components=3)
    xyz = phate_operator.fit_transform(embeddings)
    color_map = {}
    for i in range(nr_clusters):
        color_map[i] = list(np.random.choice(range(256), size=3) / 255.0)
    colors = []

    for cluster in clusters:
        colors.append(color_map[cluster])
    colors = np.asarray(colors)

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    s = [12 for _ in colors]
    ax.scatter(xyz[:, 0], xyz[:, 1], xyz[:, 2], c=colors, s=s)

    np.save(os.path.join(histogram_dir, 'FigureObject%d.npy' % model_v), xyz)
    np.save(os.path.join(histogram_dir, 'FigureObject%d_colors.npy' % model_v), colors)

    plt.show()
    print("done here")


def visualize_class_embeddings():
    model_v = 7
    num_classes = 100
    num_students = 10
    embeddings, ys = get_images_embeddings(model_v=model_v)
    cluster_path = "subtask_partitioning/cifar100_effB7_%d_img_clusters.npy" % num_students
    ys = get_image_cluster_indices(cluster_path=cluster_path)

    imgs_per_class = 600
    embeddings = embeddings[:num_classes, :imgs_per_class]
    embeddings_flatten = np.reshape(embeddings, (embeddings.shape[0] * embeddings.shape[1], embeddings.shape[2]))
    tsv_file = os.path.join(kmeans_dir, "plots/embds_%d.tsv" % model_v)
    tsv_meta_file = os.path.join(kmeans_dir, "plots/embds_meta_%d.tsv" % model_v)

    # with open(tsv_file, "w") as tsv_writer:
    #     for emb in embeddings_flatten:
    #         tsv_writer.write("\t".join(["%.8f" % itm for itm in emb]))
    #         tsv_writer.write("\n")

    with open(tsv_meta_file, "w") as tsv_writer:
        for i in ys:
            tsv_writer.write(str(i))
            tsv_writer.write("\n")
    exit()


def visualize_split_images():
    map_class_embeddings, _ = get_images_embeddings(model_v=7)
    # imgs_per_class = map_class_embeddings.shape[1]
    imgs_per_class = 20
    map_class_embeddings = map_class_embeddings[:, :imgs_per_class]
    data_flatten = np.reshape(map_class_embeddings, (
        map_class_embeddings.shape[0] * map_class_embeddings.shape[1], map_class_embeddings.shape[2]))
    # tsne_operator = TSNE(n_components=2, perplexity=8)
    # xy_2d = tsne_operator.fit_transform(data_flatten)
    phate_operator = phate.PHATE(knn=len(map_class_embeddings), n_jobs=-1, n_components=3)
    xy_2d = phate_operator.fit_transform(data_flatten)
    print("fit_transform done")

    colors = []
    for cls_idx in range(map_class_embeddings.shape[0]):
        current_color = list(np.random.choice(range(256), size=3) / 255.0)
        colors.extend([current_color for _ in range(imgs_per_class)])
    s = [12 for _ in colors]

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    ax.scatter(xy_2d[:, 0], xy_2d[:, 1], xy_2d[:, 2], c=colors, s=s)
    ax.set_xlabel('X Label')
    ax.set_ylabel('Y Label')
    ax.set_zlabel('Z Label')

    plt.show()
    print("done here")


def split_cifar_classes():
    import efficientnet.tfkeras as efn
    model_v = 7
    emb_model = EmbeddingNet(model_v=model_v)
    (X_train, y_train), (_, y_test) = cifar100.load_data()
    X_train = efn.preprocess_input(X_train)

    num_classes = len(np.unique(y_test))
    imgs_per_class = 500
    samples_per_class = imgs_per_class
    bs = 50

    for n_clusters in [20]:
        sample_embeddings = [[] for _ in range(num_classes)]

        for cls_idx in range(num_classes):
            class_idx_start = cls_idx * imgs_per_class
            class_idx_end = (cls_idx + 1) * imgs_per_class
            # img_indices = random.sample(range(class_idx_start, class_idx_end), samples_per_class)
            sample_X_train = X_train[class_idx_start: class_idx_end]
            sample_Y_train = y_train[class_idx_start: class_idx_end]
            steps_per_epoch = max(1, int(sample_Y_train.shape[0] / bs))
            for batch_idx, (image_batch, y_batch) in enumerate(
                    ImageDataGenerator().flow(sample_X_train, sample_Y_train, batch_size=bs, shuffle=False)):
                if batch_idx % 5 == 0:
                    print("cls %d batch %d/%d" % (cls_idx, batch_idx, steps_per_epoch))
                if batch_idx >= steps_per_epoch:
                    break
                y_batch = np.squeeze(y_batch)
                embeddings = emb_model(image_batch).numpy()
                for emb_idx in range(len(embeddings)):
                    sample_embeddings[y_batch[emb_idx]].append(embeddings[emb_idx])

        sample_embeddings = np.asarray(sample_embeddings)
        sample_embeddings = sample_embeddings.squeeze().mean(axis=1)
        print("sample_embeddings.shape", sample_embeddings.shape)
        kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(sample_embeddings)

        class_distances = []
        for cls_idx in range(num_classes):
            distances = kmeans.transform([sample_embeddings[cls_idx]])[0]
            class_distances.append(distances)

        class_distances = np.squeeze(np.asarray(class_distances))
        print("class_distances.shape", class_distances.shape)

        final_clustering = [[] for _ in range(n_clusters)]
        done = False
        used_classes = []
        while not done:
            for cluster_idx in range(n_clusters):
                closest_class = class_distances[:, cluster_idx].argmin()
                class_distances[closest_class, :] = 9999999
                used_classes.append(closest_class)
                final_clustering[cluster_idx].append(closest_class)
                if len(used_classes) == num_classes:
                    done = True
                    break
        print("final_%d_clustering" % n_clusters, final_clustering)
        final_clustering = np.asarray(final_clustering)
        np.save("subtask_partitioning/cifar100_effB%d_%d_raw_clusters.npy" % (model_v, n_clusters), final_clustering)

        d = {}
        for cluster_idx, cluster in enumerate(final_clustering):
            for class_from_cluster in cluster:
                d[class_from_cluster] = cluster_idx
        image_labels = []

        y = np.squeeze(np.concatenate((y_train, y_test), axis=0))
        for img_y in y:
            image_labels.append(d[img_y])
        image_labels = np.asarray(image_labels, dtype=int)
        np.save("subtask_partitioning/cifar100_effB%d_%d_img_clusters.npy" % (model_v, n_clusters), image_labels)
        print("all done")


def write_y_imagenet_mapping():
    print("start parse image net ... ")
    (X_train, y_train), (_, y_test) = parse_imagenet_raw(10, efn.preprocess_input)
    print("end parse image net")

    model_v = 7
    n_clusters = 50
    cluster_path = "subtask_partitioning/imagenet_effB%d_%d_raw_clusters.npy" % (model_v, n_clusters)
    final_clustering = np.load(cluster_path, allow_pickle=True)

    d = {}
    for cluster_idx, cluster in enumerate(final_clustering):
        for class_from_cluster in cluster:
            d[class_from_cluster] = cluster_idx
    image_labels = []

    y = np.squeeze(np.concatenate((y_train, y_test), axis=0))
    for img_y in y:
        image_labels.append(d[img_y])
    image_labels = np.asarray(image_labels, dtype=int)
    np.save("subtask_partitioning/imagenet_effB%d_%d_img_clusters.npy" % (model_v, n_clusters), image_labels)
    print("all done")


def split_food101_classes(food_101_dir):
    import efficientnet.tfkeras as efn

    model_v = 7
    bs = 50
    n_clusters = 20
    (X_train, orig_y_train), (_, y_test) = parse_food101(img_size=32,
                                                         custom_preprocess=efn.preprocess_input, data_folder=food_101_dir)

    raw_clusters_path = "subtask_partitioning/food101_effB%d_%d_raw_clusters.npy" % (model_v, n_clusters)
    if not os.path.exists(raw_clusters_path):
        print("start parse image net ... ")

        print("end parse image net")

        imgs_per_class = 200
        num_classes = len(np.unique(y_test))
        orig_y_train = np.asarray(orig_y_train)
        new_X_train = []
        new_Y_train = []
        for cl_i in range(num_classes):
            cls_indices = np.where(orig_y_train == cl_i)[0][:imgs_per_class]
            new_X_train.extend(X_train[cls_indices].tolist())
            new_Y_train.extend(orig_y_train[cls_indices].tolist())

        X_train = np.asarray(new_X_train)
        y_train = np.asarray(new_Y_train)

        sample_embeddings = [[] for _ in range(num_classes)]
        nr_batches = len(X_train) / bs
        emb_model = EmbeddingNet(model_v=model_v)
        for batch_idx, (image_batch, y_batch) in enumerate(
                ImageDataGenerator().flow(X_train, y_train, batch_size=bs, shuffle=False)):
            if batch_idx % 10 == 0:
                print("batch %d/%d" % (batch_idx, nr_batches))
            if batch_idx >= nr_batches:
                break
            y_batch = np.squeeze(y_batch)
            embeddings = emb_model(image_batch).numpy()
            for emb_idx in range(len(embeddings)):
                sample_embeddings[y_batch[emb_idx]].append(embeddings[emb_idx])
        print("~~~ done create embeddings ~~~")
        sample_embeddings_mean = []

        for s in sample_embeddings:
            if len(s) > 0:
                sample_embeddings_mean.append(np.asarray(s).mean(axis=0))

        sample_embeddings_mean = np.asarray(sample_embeddings_mean)

        print("sample_embeddings.shape", sample_embeddings_mean.shape)
        kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(sample_embeddings_mean)

        num_classes = len(sample_embeddings_mean)
        print("num_classes ", num_classes)
        class_distances = []
        for batch_idx in range(num_classes):
            distances = kmeans.transform([sample_embeddings_mean[batch_idx]])[0]
            class_distances.append(distances)

        class_distances = np.squeeze(np.asarray(class_distances))
        print("class_distances.shape", class_distances.shape)

        final_clustering = [[] for _ in range(n_clusters)]
        done = False
        used_classes = []
        while not done:
            for cluster_idx in range(n_clusters):
                closest_class = class_distances[:, cluster_idx].argmin()
                class_distances[closest_class, :] = 9999999
                used_classes.append(closest_class)
                final_clustering[cluster_idx].append(closest_class)
                if len(used_classes) == num_classes:
                    done = True
                    break
        print("final_%d_clustering" % n_clusters, final_clustering)
        final_clustering = np.asarray(final_clustering)
        np.save(raw_clusters_path, final_clustering)
    else:
        final_clustering = np.load(raw_clusters_path, allow_pickle=True)

    d = {}
    for cluster_idx, cluster in enumerate(final_clustering):
        for class_from_cluster in cluster:
            d[class_from_cluster] = cluster_idx
    image_labels = []

    y = np.squeeze(np.concatenate((orig_y_train, y_test), axis=0))
    for img_y in y:
        image_labels.append(d[img_y])
    image_labels = np.asarray(image_labels, dtype=int)

    print("final label count", len(image_labels))
    np.save("subtask_partitioning/food101_effB%d_%d_img_clusters.npy" % (model_v, n_clusters), image_labels)
    print("all done")


def split_imagenet_classes():
    import efficientnet.tfkeras as efn

    model_v = 7
    bs = 50
    n_clusters = 25
    raw_clusters_path = "subtask_partitioning/imagenet_effB%d_%d_raw_clusters.npy" % (model_v, n_clusters)
    (X_train, orig_y_train), (_, y_test) = parse_imagenet_raw(10, efn.preprocess_input)

    if not os.path.exists(raw_clusters_path):
        print("start parse image net ... ")

        print("end parse image net")

        imgs_per_class = 200
        num_classes = len(np.unique(y_test))
        orig_y_train = np.asarray(orig_y_train)
        new_X_train = []
        new_Y_train = []
        for cl_i in range(num_classes):
            cls_indices = np.where(orig_y_train == cl_i)[0][:imgs_per_class]
            new_X_train.extend(X_train[cls_indices].tolist())
            new_Y_train.extend(orig_y_train[cls_indices].tolist())

        X_train = np.asarray(new_X_train)
        y_train = np.asarray(new_Y_train)

        sample_embeddings = [[] for _ in range(num_classes)]
        nr_batches = len(X_train) / bs
        emb_model = EmbeddingNet(model_v=model_v)
        for batch_idx, (image_batch, y_batch) in enumerate(
                ImageDataGenerator().flow(X_train, y_train, batch_size=bs, shuffle=False)):
            if batch_idx % 10 == 0:
                print("batch %d/%d" % (batch_idx, nr_batches))
            if batch_idx >= nr_batches:
                break
            y_batch = np.squeeze(y_batch)
            embeddings = emb_model(image_batch).numpy()
            for emb_idx in range(len(embeddings)):
                sample_embeddings[y_batch[emb_idx]].append(embeddings[emb_idx])
        print("~~~ done create embeddings ~~~")
        sample_embeddings_mean = []

        for s in sample_embeddings:
            if len(s) > 0:
                sample_embeddings_mean.append(np.asarray(s).mean(axis=0))

        sample_embeddings_mean = np.asarray(sample_embeddings_mean)

        print("sample_embeddings.shape", sample_embeddings_mean.shape)
        kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(sample_embeddings_mean)

        num_classes = len(sample_embeddings_mean)
        print("num_classes ", num_classes)
        class_distances = []
        for batch_idx in range(num_classes):
            distances = kmeans.transform([sample_embeddings_mean[batch_idx]])[0]
            class_distances.append(distances)

        class_distances = np.squeeze(np.asarray(class_distances))
        print("class_distances.shape", class_distances.shape)

        final_clustering = [[] for _ in range(n_clusters)]
        done = False
        used_classes = []
        while not done:
            for cluster_idx in range(n_clusters):
                closest_class = class_distances[:, cluster_idx].argmin()
                class_distances[closest_class, :] = 9999999
                used_classes.append(closest_class)
                final_clustering[cluster_idx].append(closest_class)
                if len(used_classes) == num_classes:
                    done = True
                    break
        print("final_%d_clustering" % n_clusters, final_clustering)
        final_clustering = np.asarray(final_clustering)
        np.save(raw_clusters_path, final_clustering)
    else:
        final_clustering = np.load(raw_clusters_path, allow_pickle=True)

    d = {}
    for cluster_idx, cluster in enumerate(final_clustering):
        for class_from_cluster in cluster:
            d[class_from_cluster] = cluster_idx
    image_labels = []

    y = np.squeeze(np.concatenate((orig_y_train, y_test), axis=0))
    for img_y in y:
        image_labels.append(d[img_y])
    image_labels = np.asarray(image_labels, dtype=int)

    print("final label count", len(image_labels))
    np.save("subtask_partitioning/imagenet_effB%d_%d_img_clusters.npy" % (model_v, n_clusters), image_labels)
    print("all done")


def test_split():
    classes = np.load("subtask_partitioning/cifar100_effB7_10_raw_clusters.npy", allow_pickle=True)

    with open("cifar100_cls_name", "r") as file:
        content = file.read()
        class_name = content.split(',')

    for cluster in classes:
        print("\ncluster: ", end='\n')
        for cls in cluster:
            print(class_name[cls], end=",")


def show_3d_plot():
    model_v = 0
    num_classes = 50
    in_dir = os.path.join(kmeans_dir, "plots")
    xyz = np.load(os.path.join(in_dir, 'cifar_classes_%d_emb%d.npy' % (num_classes, model_v)), allow_pickle=True)
    colors = np.load(os.path.join(in_dir, 'cifar_classes_%d_emb%d_colors.npy' % (num_classes, model_v)),
                     allow_pickle=True)

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    s = [12 for _ in colors]
    ax.scatter(xyz[:, 0], xyz[:, 1], xyz[:, 2], c=colors, s=s)
    plt.show()


def get_max_indices_from_matrix(m):
    max_v = np.max(m)
    for i in range(m.shape[0] - 1):
        for j in range(i, m.shape[1]):
            if m[i][j] == max_v:
                return i, j
    return -1, -1


def intersect_splittings():
    model_v1 = 5
    model_v2 = 7
    ds_name = "cifar100"
    clusters_list_v1 = get_image_cluster_indices(
        cluster_path="E:/ensemble_learning/tf1_like/subtask_partitioning/kmeans")
    clusters_list_v2 = get_image_cluster_indices(
        cluster_path="E:/ensemble_learning/tf1_like/subtask_partitioning/kmeans")
    num_clusters = len(np.unique(clusters_list_v1))
    clusters_v1 = {}
    clusters_v2 = {}

    for cluster in range(num_clusters):
        clusters_v1[cluster] = np.where(clusters_list_v1 == cluster)
        clusters_v2[cluster] = np.where(clusters_list_v2 == cluster)
    clusters_intersection = [np.asarray([0 for _ in range(num_clusters)]) for _ in range(num_clusters)]
    for idx_model1 in range(num_clusters - 1):
        for idx_model2 in range(idx_model1, num_clusters):
            intersection = len(np.intersect1d(clusters_v1[idx_model1], clusters_v2[idx_model2]))
            clusters_intersection[idx_model1][idx_model2] = intersection
    clusters_intersection = np.array(clusters_intersection)
    cluster_matches = []

    for _ in range(num_clusters):
        i, j = get_max_indices_from_matrix(clusters_intersection)
        cluster_matches.append((i, j, clusters_intersection[i][j]))
        clusters_intersection[i, :] = 0
        clusters_intersection[:, j] = 0
    print("cluster_matches", cluster_matches)
    print("")


if __name__ == "__main__":
    # intersect_splittings()
    # split_unbalanced_images()
    # split_unbalanced_classes()
    # split_cifar_classes()
    # split_imagenet_classes()
    food_101_dir = ""
    split_food101_classes(food_101_dir)
    # write_y_imagenet_mapping()
    # test_split()
    # visualize_class_embeddings()
    # show_3d_plot()

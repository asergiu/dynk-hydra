import csv
import glob
import pickle
import os

import numpy as np

from tools.geometry_utils import make_image_square
from tools.utils import make_sure_dir_exists


def parse_imgnet_deprecated():
    rows = []
    with open('LOC_train_solution.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for r_idx, row in enumerate(csv_reader):
            if r_idx == 0:
                continue
            rows.append(int(row[0].split("_")[1]))
    rows = sorted(rows)


def unpickle(file):
    with open(file, 'rb') as fo:
        dict = pickle.load(fo)
    return dict


def parse_imagenet_32(data_file, img_size=32):
    d = unpickle(data_file)
    x = d['data']
    y = d['labels']
    mean_image = d['mean']

    # for i in range(0, len(x)):
    #     cv2.imshow("i", x[i])
    #     cv2.waitKey(0)

    x = x / np.float32(255)
    mean_image = mean_image / np.float32(255)

    # Labels are indexed from 1, shift it so that indexes start at 0
    y = [i - 1 for i in y]
    data_size = x.shape[0]

    x -= mean_image

    img_size2 = img_size * img_size
    x = np.dstack((x[:, :img_size2], x[:, img_size2:2 * img_size2], x[:, 2 * img_size2:]))
    x = x.reshape((x.shape[0], img_size, img_size, 3)).transpose(0, 3, 1, 2)

    # create mirrored images
    X_train = x[0:data_size, :, :, :]
    Y_train = y[0:data_size]
    return dict(X_train=X_train, Y_train=Y_train.astype('int32'), mean=mean_image)


def prepare_food_101(in_dir, out_dir):
    from cv2 import cv2

    make_sure_dir_exists(out_dir)
    files = glob.glob(os.path.join(in_dir, "**/*.jpg"), recursive=True)
    for in_file in files:
        dir_name = os.path.dirname(in_file).split("/")[-1]
        out_path = os.path.join(out_dir, dir_name, os.path.basename(in_file))
        print("process", in_file)

        class_dir = os.path.join(out_dir, dir_name)
        make_sure_dir_exists(class_dir)

        img = cv2.imread(in_file)
        img = make_image_square(img, 32, cv2.BORDER_CONSTANT, 0)

        cv2.imwrite(out_path, img)
        # print(out_path)


if __name__ == "__main__":

    prepare_food_101(in_dir="Z:/image_databases/food-101/images/", out_dir="Z:/image_databases/food-101/images32")
    print("done")

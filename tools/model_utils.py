from tensorflow import keras
from tensorflow.keras import layers
import os


def get_model_using_input(img_inputs, num_classes, conv_depths, model_name):
    x = img_inputs
    x = layers.experimental.preprocessing.Rescaling(1.0 / 255)(x)
    for conv_depth in conv_depths:
        x = layers.Conv2D(conv_depth, 3, activation="relu")(x)

    x = layers.GlobalAveragePooling2D()(x)
    x = layers.Dense(256, activation="relu")(x)
    x = layers.Dropout(0.5)(x)
    outputs = layers.Dense(num_classes)(x)

    model = keras.Model(inputs=img_inputs, outputs=outputs)

    return outputs, model


def make_sure_dir_structure_exist(dir1):
    individual_dirs = dir1.split("/")
    if not os.path.exists(dir1):
        os.mkdir(dir1)
        print("create dir", dir1)


def model_version(v):
    import efficientnet.tfkeras as efn
    if v == 0:
        return efn.EfficientNetB0
    if v == 1:
        return efn.EfficientNetB1
    if v == 2:
        return efn.EfficientNetB2
    if v == 3:
        return efn.EfficientNetB3
    if v == 4:
        return efn.EfficientNetB4
    if v == 5:
        return efn.EfficientNetB5
    if v == 6:
        return efn.EfficientNetB6
    if v == 7:
        return efn.EfficientNetB7

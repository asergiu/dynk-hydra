import os
import random

import numpy as np
import tensorflow as tf

from tensorflow.keras.datasets import cifar10, cifar100


class GaterAccuracy(tf.keras.metrics.Metric):

    def __init__(self, **kwargs):
        super(GaterAccuracy, self).__init__(**kwargs)
        self.accuracy_obj = tf.keras.metrics.Accuracy(name="gaterAcc")
        self.class_branch_map = lookup_table_cifar("cifar100", num_students=num_students)

    def update_state(self, y_true_one_hot, y_pred, sample_weight=None):
        y_true = tf.argmax(y_true_one_hot, axis=-1)
        print("y_true", y_true)
        gt_branch = self.class_branch_map.lookup(tf.cast(y_true, dtype='int32'))
        y_cls = tf.math.argmax(y_pred, axis=1)
        print("y_cls", y_cls)
        print("gt_branch", gt_branch)
        self.accuracy_obj.update_state(gt_branch, y_cls)

    def result(self):
        return self.accuracy_obj.result()

    def reset_states(self):
        self.accuracy_obj.reset_states()


class StudentAccuracy(tf.keras.metrics.Metric):

    def __init__(self, idx, name='acc_', **kwargs):
        super(StudentAccuracy, self).__init__(name=name + str(idx), **kwargs)
        self.idx = idx
        self.accuracy_obj = tf.keras.metrics.Accuracy(name='acc_%d' % self.idx)

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_cls = tf.math.argmax(y_pred[:, self.idx, :], axis=1)
        self.accuracy_obj.update_state(y_true, y_cls)

    def result(self):
        return self.accuracy_obj.result()

    def reset_states(self):
        self.accuracy_obj.reset_states()


class LRTensorBoard(tf.keras.callbacks.Callback):
    def __init__(self):
        super().__init__()
        self._supports_tf_logs = True

    def on_epoch_end(self, epoch, logs=None):
        logs["learning_rate"] = self.model.optimizer.lr


def calculate_acc(current_gt, current_pred):
    accuracy_obj = tf.keras.metrics.Accuracy()
    accuracy_obj.update_state(current_gt, current_pred)
    accuracy = accuracy_obj.result()
    return accuracy


def get_accuracies(seeds, final_predictions, ys):
    ys = np.asarray(ys)
    predictors_accuracies = []
    for predictor_idx in range(len(seeds) + 1):
        current_pred = final_predictions[predictor_idx]
        if len(current_pred) > 0:
            current_gt = ys
            accuracy = calculate_acc(current_gt, current_pred)
            predictors_accuracies.append(accuracy)

    return predictors_accuracies


def get_image_cluster_indices(cluster_path=None):
    clusters = np.load(cluster_path, allow_pickle=True)
    return clusters.astype(np.int32)


def lookup_table_cifar(ds_name, return_python_map=False, num_students=10):
    keys = []
    values = []
    d = {}

    ds = cifar10.load_data() if ds_name == "cifar10" else cifar100.load_data()
    (_, y_train), (_, y_test) = ds
    y = np.squeeze(np.concatenate((y_train, y_test), axis=0))

    if ds_name == "cifar10":
        keys = [4, 5, 3, 7, 1, 8, 9, 0, 2, 6]
        values = [0, 0, 0, 0, 1, 1, 1, 2, 2, 2]
        for idx in range(len(keys)):
            d[keys[idx]] = values[idx]

    else:
        if num_students == 10 or num_students == 5 or num_students == 3:
            all_classes = np.load("../../tools/cifar100_subtask_%d_clusters.npy" % num_students, allow_pickle=True)
            for cluster_idx, cluster in enumerate(all_classes):
                keys.extend(cluster)
                values.extend([cluster_idx for _ in range(len(cluster))])
                # if return_python_map:
                for class_from_cluster in cluster:
                    d[class_from_cluster] = cluster_idx
            image_labels = []
            for img_y in y:
                image_labels.append(d[img_y])
            image_labels = np.asarray(image_labels, dtype=int)
            np.save("../../tools/subtask_partitioning/cifar100_balanced_fixed_%d_clusters.npy" % num_students,
                    image_labels)
            # exit(0)

        elif num_students == 20:
            with open("../coarse_cifar100.txt", "r") as coarse_file:
                content_coarse = coarse_file.readlines()

            with open("../cifar100_cls_name", "r") as names_file:
                names_line = names_file.readline()
                names_list = names_line.split(", ")
                names_list = [item.strip() for item in names_list]

            for cluster_idx, cluster_line in enumerate(content_coarse):
                cluster_list = cluster_line.split(",")
                for class_from_cluster in cluster_list:
                    class_to_name_idx = names_list.index(class_from_cluster.strip())
                    keys.append(class_to_name_idx)
                    values.append(cluster_idx)
                    d[class_to_name_idx] = cluster_idx
            image_labels = []

            for img_y in y:
                image_labels.append(d[img_y])
            image_labels = np.asarray(image_labels, dtype=int)
            np.save("../../tools/subtask_partitioning/cifar100_balanced_fixed_%d_clusters.npy" % num_students,
                    image_labels)

    table = tf.lookup.StaticHashTable(
        initializer=tf.lookup.KeyValueTensorInitializer(
            keys=tf.constant(keys, dtype='int32'),
            values=tf.constant(values), ), default_value=tf.constant(-1),
    )
    if return_python_map:
        return d
    return table


def get_num_classes(ds_name):
    if ds_name == "cifar10":
        return 10
    elif ds_name == "cifar100":
        return 100
    elif ds_name == "food101":
        return 101
    elif ds_name == "imagenet":
        return 1000


def get_num_studs(ds_name):
    if ds_name == "cifar10":
        return 3
    elif ds_name == "cifar100" or ds_name == "food101":
        return 20
    elif ds_name == "imagenet":
        return 25


def make_sure_dir_exists(dir1):
    if not os.path.exists(dir1):
        os.mkdir(dir1)
        print("create dir", dir1)


def make_sure_dirs_exists(dirs):
    for dir1 in dirs:
        make_sure_dir_exists(dir1)


def create_original_coarse_for_cifar100():
    ds_name = "cifar100"
    num_students = 20
    cluster_path_train = "../../tools/subtask_partitioning/%s_effB7_%d_img_clusters_train_random.npy" % (
    ds_name, num_students)
    cluster_path_test = "../../tools/subtask_partitioning/%s_effB7_%d_img_clusters_test_random.npy" % (
    ds_name, num_students)

    ds = cifar10.load_data() if ds_name == "cifar10" else cifar100.load_data()
    (_, y_train), (_, y_test) = ds
    y_train = np.squeeze(y_train)
    y_test = np.squeeze(y_test)
    keys = []
    values = []
    d = {}
    with open("../coarse_cifar100.txt", "r") as coarse_file:
        content_coarse = coarse_file.readlines()

    with open("../cifar100_cls_name", "r") as names_file:
        names_line = names_file.readline()
        names_list = names_line.split(", ")
        names_list = [item.strip() for item in names_list]

    for cluster_idx, cluster_line in enumerate(content_coarse):
        cluster_list = cluster_line.split(",")
        for class_from_cluster in cluster_list:
            class_to_name_idx = names_list.index(class_from_cluster.strip())
            keys.append(class_to_name_idx)
            values.append(cluster_idx)
            d[class_to_name_idx] = cluster_idx

    image_labels_train = []
    for img_y in y_train:
        image_labels_train.append(d[img_y])
    image_labels_train = np.asarray(image_labels_train, dtype=int)

    image_labels_test = []
    for img_y in y_test:
        image_labels_test.append(d[img_y])
    image_labels_test = np.asarray(image_labels_test, dtype=int)

    # np.save(cluster_path_train, image_labels_train)
    # np.save(cluster_path_test, image_labels_test)


def create_random_coarse_for_cifar100():
    ds_name = "cifar100"
    num_students = 20
    num_classes = 100
    cluster_path_train = "../../tools/subtask_partitioning/%s_effB7_%d_img_clusters_train_random.npy" % (
        ds_name, num_students)
    cluster_path_test = "../../tools/subtask_partitioning/%s_effB7_%d_img_clusters_test_random.npy" % (
        ds_name, num_students)

    ds = cifar10.load_data() if ds_name == "cifar10" else cifar100.load_data()
    (_, y_train), (_, y_test) = ds
    y_train = np.squeeze(y_train)
    y_test = np.squeeze(y_test)
    keys = []
    values = []
    d = {}

    occupy_list = []
    for i in range(num_students):
        occupy_list.append([])

    for i in range(num_classes):
        done = False
        while not done:
            cluster_idx = random.randrange(0, num_students)
            if len(occupy_list[cluster_idx]) < int(num_classes / num_students):
                occupy_list[cluster_idx].append(i)
                done = True

    for cluster_idx, cluster_list in enumerate(occupy_list):
        for cl_idx in cluster_list:
            d[cl_idx] = cluster_idx

    image_labels_train = []
    for img_y in y_train:
        image_labels_train.append(d[img_y])
    image_labels_train = np.asarray(image_labels_train, dtype=int)

    image_labels_test = []
    for img_y in y_test:
        image_labels_test.append(d[img_y])
    image_labels_test = np.asarray(image_labels_test, dtype=int)

    np.save(cluster_path_train, image_labels_train)
    np.save(cluster_path_test, image_labels_test)


if __name__ == "__main__":
    # create_original_coarse_for_cifar100()
    create_random_coarse_for_cifar100()

    # lookup_table_cifar("cifar100")
    # w = tf.constant([
    #     [[0], [0], [1], [1]],
    #     [[1], [1], [0], [0]]
    # ])
    # w = tf.squeeze(w)
    # values = tf.constant([[
    #     [110, 120, 130],
    #     [1110, 1120, 1130],
    #     [2110, 2120, 2130],  # -
    #     [3110, 3120, 3130],  # -
    # ], [
    #     [11, 12, 13],  # -
    #     [111, 112, 113],  # -
    #     [211, 212, 213],
    #     [311, 312, 313],
    # ]])
    # indices = tf.where(w)
    # print(indices)
    # gath = tf.gather(values, indices, axis=1)
    # print(indices)

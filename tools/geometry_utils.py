

def make_image_square(image, square_size, border_type, border_value):
    from cv2 import cv2
    height, width = image.shape[0], image.shape[1]
    top, bot = 0, 0
    left, right = 0, 0
    if height > width:
        largest = height
        top = 0
        bot = 0
        left, right = (largest - width) // 2, (largest - width) // 2
    else:
        largest = width
        top, bot = (largest - height) // 2, (largest - height) // 2
        left, right = 0, 0

    image_square = cv2.copyMakeBorder(image, top, bot, left, right, border_type, border_value)  # np.zeros((largest,largest), dtype="uint8")
    x_pos = left
    y_pos = bot

    image_square[y_pos:y_pos + height, x_pos:x_pos + width] = image
    image_square = cv2.resize(image_square, (square_size, square_size), interpolation=cv2.INTER_AREA)

    return image_square
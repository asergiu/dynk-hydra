from abc import ABC

import numpy as np
import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.python.keras.utils import losses_utils


class sparse_categorical_crossentropy_ens_only_tf1(keras.losses.Loss):

    def __init__(self, num_studs, name="sparse_categorical_crossentropy_ens_only", from_logits_ensemble=True):
        super().__init__(name=name)
        self.num_studs = num_studs
        self.from_logits_ensemble = from_logits_ensemble

    def call(self, y_true, y_pred):
        loss = tf.reduce_mean(keras.backend.sparse_categorical_crossentropy(y_true, y_pred[:, self.num_studs, :], from_logits=self.from_logits_ensemble))
        return loss


class KD_projection_loss(keras.losses.Loss):
    def __init__(self):
        super().__init__()

    @tf.function
    def call(self, y_true, y_pred):
        return y_pred


class KL_knowledge_distilation(keras.losses.Loss):
    def __init__(self, teacher_output=None, student_output=None, softmax_temperature=1, name="KL"):
        super().__init__(name=name)
        self.teacher_output = teacher_output
        self.student_output = student_output
        self.softmax_temperature = softmax_temperature
        self.distillations_loss_fn = keras.losses.KLDivergence(losses_utils.ReductionV2.NONE)

    @tf.function
    def call(self, teacher, student):
        # student_with_softmax = tf.nn.softmax(self.student_output / self.softmax_temperature, axis=1)
        # teacher_with_softmax = tf.nn.softmax(self.teacher_output / self.softmax_temperature, axis=1)

        # kl_val = self.distillations_loss_fn(teacher_with_softmax, student_with_softmax)
        kl_val = self.distillations_loss_fn(teacher, student)

        return tf.reduce_mean(kl_val)


class StudentBinarySubtaskLoss(keras.losses.Loss, ABC):
    def __init__(self, stud_idx, class_branch_map, name, from_logits=True, num_classes=100, good_sample_weight=0.05):
        super().__init__(name=name)
        self.stud_idx = stud_idx
        self.good_sample_weight = good_sample_weight
        self.num_classes = num_classes
        self.class_branch_map = class_branch_map

        self.cc = keras.losses.BinaryCrossentropy(from_logits=from_logits, reduction=tf.keras.losses.Reduction.AUTO)

    @tf.function
    def call(self, y_true_one_hot, y_pred):
        y_true = tf.argmax(y_true_one_hot, axis=-1)
        gt_branch = self.class_branch_map.lookup(tf.cast(y_true, dtype='int32'))

        y_true_selected = tf.where(gt_branch == self.stud_idx, tf.ones_like(y_true), tf.zeros_like(y_true))
        if self.good_sample_weight is None:
            sample_weight = None
        else:
            good_sample_weight_list = tf.ones_like(y_true, dtype=tf.float32) * tf.constant(self.good_sample_weight, dtype=tf.float32)
            sample_weight = tf.where(gt_branch != self.stud_idx, tf.ones_like(y_true, dtype=tf.float32), good_sample_weight_list)

        y_true_selected_one_hot = tf.one_hot(y_true_selected, depth=2)
        batch_losses = self.cc(y_true_selected_one_hot, y_pred, sample_weight=sample_weight)

        return batch_losses


class StudentZeroisationProjectionLoss(keras.losses.Loss):
    def __init__(self, stud_idx, name):
        super().__init__(name=name)
        self.stud_idx = stud_idx

    @tf.function
    def call(self, y_true, y_pred):
        # y_true_class = y_true[:, 0]
        gt_branch = y_true[:, 1]

        branch_indices = tf.where(gt_branch == self.stud_idx)
        non_branch_indices = tf.squeeze(tf.where(gt_branch != self.stud_idx))

        selection_indices = tf.random.uniform(shape=[tf.math.maximum(tf.cast(tf.shape(branch_indices)[0] / 2, tf.int32), 1)], minval=0, maxval=tf.shape(non_branch_indices)[0],
                                              dtype=tf.int32)
        selection_indices_unique = tf.unique(selection_indices)[0]
        non_branch_indices_selected = tf.gather(non_branch_indices, selection_indices_unique)
        selected_projections = tf.gather(y_pred, non_branch_indices_selected)
        # batch_losses = tf.reduce_sum(selected_projections)
        batch_losses_abs = tf.reduce_sum(tf.abs(selected_projections))
        return batch_losses_abs


class StudentImageSubtaskLoss(keras.losses.Loss):
    def  __init__(self, stud_idx, name, from_logits=True, extra_class=False):
        super().__init__(name=name)
        self.stud_idx = stud_idx
        self.extra_class = extra_class
        reduction = tf.keras.losses.Reduction.AUTO if extra_class else tf.keras.losses.Reduction.NONE
        self.scc = keras.losses.SparseCategoricalCrossentropy(from_logits=from_logits, reduction=reduction)

    @tf.function
    def call(self, y_true, y_pred):
        y_true_class = y_true[:, 0]
        gt_branch = y_true[:, 1]

        selected_indices = tf.where(gt_branch == self.stud_idx)
        # selected_indices = tf.squeeze(selected_indices)

        y_true_selected = tf.gather(y_true_class, selected_indices)
        y_pred_selected = tf.gather(y_pred, selected_indices)
        batch_losses = self.scc(y_true_selected, y_pred_selected)

        batch_losses = tf.reduce_mean(batch_losses)
        batch_losses = tf.where(tf.math.is_nan(batch_losses), tf.zeros_like(batch_losses), batch_losses)

        return batch_losses


class StudentSubtaskLoss(keras.losses.Loss):
    def __init__(self, stud_idx, class_branch_map, name, from_logits=True, num_classes=100, extra_class=False, good_sample_weight=0.05):
        super().__init__(name=name)
        self.stud_idx = stud_idx
        self.good_sample_weight = good_sample_weight
        self.num_classes = num_classes
        self.extra_class = extra_class
        self.class_branch_map = class_branch_map
        reduction = tf.keras.losses.Reduction.AUTO if extra_class else tf.keras.losses.Reduction.NONE
        self.cc = keras.losses.CategoricalCrossentropy(from_logits=from_logits, reduction=reduction)

    @tf.function
    def call(self, y_true_one_hot, y_pred):
        if self.extra_class:
            return self.call_extra_class(y_true_one_hot, y_pred)
        return self.call_normal(y_true_one_hot, y_pred)

    @tf.function
    def call_extra_class(self, y_true_one_hot, y_pred):
        y_true = tf.argmax(y_true_one_hot, axis=-1)

        y_true_100 = tf.ones_like(y_true) * (self.num_classes - 1)
        gt_branch = self.class_branch_map.lookup(tf.cast(y_true, dtype='int32'))
        y_true_selected = tf.where(gt_branch == self.stud_idx, y_true, y_true_100)

        good_sample_w_list = tf.ones_like(y_true, dtype=tf.float32) * tf.constant(self.good_sample_weight, dtype=tf.float32)
        sample_weight = tf.where(gt_branch != self.stud_idx, tf.ones_like(y_true, dtype=tf.float32), good_sample_w_list)

        y_true_selected_one_hot = tf.one_hot(y_true_selected, depth=self.num_classes)
        batch_losses = self.cc(y_true_selected_one_hot, y_pred, sample_weight=sample_weight)
        return batch_losses

    @tf.function
    def call_normal(self, y_true_one_hot, y_pred):
        y_true = tf.argmax(y_true_one_hot, axis=-1)
        gt_branch = self.class_branch_map.lookup(tf.cast(y_true, dtype='int32'))
        batch_losses = self.cc(y_true_one_hot, y_pred)
        selected_indices = tf.where(gt_branch == self.stud_idx)  # batch_losses, max_loss)
        selected_losses = tf.gather(batch_losses, selected_indices)
        batch_losses = tf.reduce_mean(selected_losses)
        batch_losses = tf.where(tf.math.is_nan(batch_losses), tf.zeros_like(batch_losses), batch_losses)
        return batch_losses


class StudentSubtaskLossLayer(keras.layers.Layer):
    def __init__(self, class_branch_map, w, stud_idx, name="stud_subtask_loss_layer", **kwargs):
        super().__init__(name=name, **kwargs)
        self.class_branch_map = class_branch_map
        self.w = w
        self.stud_idx = stud_idx
        self.student_subtask_loss = StudentSubtaskLoss(stud_idx, class_branch_map, name=name)

    def call(self, inputs, **kwargs):
        #  the input of this layer is the output gate layer
        #  input is [student(None, 3, 10); y_true(None, num_classes)]

        student_pred = inputs[0]
        y_true_one_hot = inputs[1]

        batch_losses = self.student_subtask_loss(y_true=y_true_one_hot, y_pred=student_pred)
        final_loss = self.w * batch_losses
        self.add_loss(final_loss)

        return inputs[0]

    def get_config(self):
        config = super().get_config().copy()
        return config


class ImageSubtaskCrossentropyLoss(keras.losses.Loss):
    def __init__(self, from_logits=True, sparse=True, num_classes=100):
        super().__init__()
        self.sparse = sparse
        self.num_classes = num_classes
        if sparse:
            self.cc = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=from_logits)
        else:
            self.cc = tf.keras.losses.CategoricalCrossentropy(from_logits=from_logits)

    @tf.function
    def call(self, y_true_and_gt_branch, y_pred):
        y_true = y_true_and_gt_branch[:, 0]
        if not self.sparse:
            y_true = tf.one_hot(y_true, self.num_classes)

        loss = self.cc(y_true, y_pred)
        return loss


class GateImageSubtaskLoss(keras.losses.Loss):
    def __init__(self, from_logits=True):
        super().__init__()
        self.cc = keras.losses.SparseCategoricalCrossentropy(from_logits=from_logits)

    @tf.function
    def call(self, y_true_and_gt_branch, pred_weights):
        gt_branch = y_true_and_gt_branch[:, 1]
        loss = self.cc(y_true=gt_branch, y_pred=pred_weights)
        return loss


class IsInTokKGateLoss(keras.losses.Loss):
    def __init__(self, name, k=4):
        super().__init__(name=name)
        self.k = k

    @tf.function
    def call(self, y_true_one_hot, pred_weights):
        gt_branch = y_true_one_hot[:, 1]

        loss_val = 1 - tf.cast(tf.math.in_top_k(gt_branch, pred_weights, self.k), tf.float32)
        reduced_loss = tf.reduce_mean(loss_val)
        return reduced_loss


class GateSubtaskLossTopK(keras.losses.Loss):
    def __init__(self, class_branch_map, name, k=4):
        super().__init__(name=name)
        self.class_branch_map = class_branch_map
        self.k = k

    @tf.function
    def call(self, y_true_one_hot, pred_weights):
        y_true = tf.argmax(y_true_one_hot, axis=-1)
        gt_branch = self.class_branch_map.lookup(tf.cast(y_true, dtype='int32'))
        loss_val = 1 - tf.cast(tf.math.in_top_k(gt_branch, pred_weights, self.k), tf.float32)
        reduced_loss = tf.reduce_mean(loss_val)
        return reduced_loss


class GateSubtaskLoss(keras.losses.Loss):
    def __init__(self, class_branch_map, name, from_logits=True, num_clusters=10):
        super().__init__(name=name)
        self.class_branch_map = class_branch_map
        self.num_clusters = num_clusters
        self.cc = keras.losses.CategoricalCrossentropy(from_logits=from_logits, reduction=tf.keras.losses.Reduction.NONE)

    @tf.function
    def call(self, y_true_one_hot, pred_weights):
        y_true = tf.argmax(y_true_one_hot, axis=-1)
        y_true_branch = self.class_branch_map.lookup(tf.cast(y_true, dtype='int32'))
        gt_branch_one_hot = tf.one_hot(y_true_branch, self.num_clusters)
        loss = self.cc(y_true=gt_branch_one_hot, y_pred=pred_weights)
        reduced_loss = tf.reduce_mean(loss)
        return reduced_loss


class BinaryLossLayer(keras.layers.Layer):
    def __init__(self, stud_idx, binary_loss_w, **kwargs):
        super().__init__(**kwargs, name="sss" + str(stud_idx))
        self.stud_idx = stud_idx
        self.binary_loss_w = binary_loss_w
        self.cc = keras.losses.CategoricalCrossentropy(from_logits=True)

    @tf.function
    def call(self, inputs, **kwargs):
        binary_pred = inputs[0]
        stud_pred = inputs[1]
        y_true_one_hot = inputs[2]

        y_true = tf.argmax(y_true_one_hot)
        student_pred = tf.argmax(stud_pred, axis=1)

        binary_gt = tf.where(y_true == student_pred, tf.ones(tf.shape(binary_pred)[0], dtype=tf.int32), tf.zeros(tf.shape(binary_pred)[0], dtype=tf.int32))
        binary_gt_one_hot = tf.one_hot(binary_gt, 2)
        loss = self.cc(y_true=binary_gt_one_hot, y_pred=binary_pred)

        # self.add_loss(loss)
        return binary_pred


class GateSubtaskLossLayer(keras.layers.Layer):
    def __init__(self, class_branch_map, num_studs, name="gate_subtask_loss_layer", w_gate=1, **kwargs):
        super().__init__(name=name, **kwargs)
        self.class_branch_map = class_branch_map
        self.w_gate = w_gate
        self.num_studs = num_studs
        self.scc = keras.losses.SparseCategoricalCrossentropy(from_logits=True)

    def call(self, inputs, **kwargs):
        #  the input of this layer is the output gate layer
        #  input is [pred_weights(None, 3, 10); y_true(None, num_classes)]
        pred_weights = inputs[0][:, :, 0]
        y_true_one_hot = inputs[1][:, 0, :]
        y_true = tf.argmax(y_true_one_hot, axis=-1)
        gt_branch = self.class_branch_map.lookup(tf.cast(y_true, dtype='int32'))
        loss = self.scc(y_true=gt_branch, y_pred=pred_weights)
        loss = self.w_gate * loss
        self.add_loss(loss)

        return pred_weights

    def get_config(self):
        config = super().get_config().copy()
        return config


class correct_subtask_weight_loss_tf1(keras.losses.Loss):
    def __init__(self, class_branch_map, num_branches=3, name="subtask_weights_loss"):
        super().__init__(name=name)
        self.class_branch_map = class_branch_map
        self.num_branches = num_branches

    def call(self, y_true, y_pred):
        # input: y_pred is [stud_out, stud_out, stud_out, mean_out, w1, w2, w3]
        pred_weights = y_pred[:, self.num_branches + 1:, 0]  # we take just one from each weight array, we know that is expended to match the num class
        gt_branch = tf.squeeze(self.class_branch_map.lookup(tf.cast(y_true, dtype='int32')))
        loss = tf.reduce_mean(keras.losses.sparse_categorical_crossentropy(y_true=gt_branch, y_pred=pred_weights, from_logits=True))
        return loss


class sparse_categorical_crossentropy_subtask_multi_output_tf1(keras.losses.Loss):
    def __init__(self, num_studs, from_logits_students, ensemble_cce_weight, class_branch_map,
                 name="scc_subtask",
                 from_logits_ensemble=True,
                 cce_students_only=False):
        super().__init__(name=name)
        self.num_studs = num_studs
        self.from_logits_students = from_logits_students
        self.ensemble_cce_weight = ensemble_cce_weight
        self.from_logits_ensemble = from_logits_ensemble
        self.class_branch_map = class_branch_map
        self.cce_students_only = cce_students_only

    def call(self, y_true, y_pred):
        #  y_pred = [stud_0, stud_1, stud_2, ensemble, w1_0, w_1, w_2]

        losses = []
        for i in range(self.num_studs):
            #  here we penalize if a model predict correctly on a class which is not from its cluster
            gt_branch = tf.squeeze(self.class_branch_map.lookup(tf.cast(y_true, dtype='int32')))
            batch_losses = keras.backend.sparse_categorical_crossentropy(y_true, y_pred[:, i, :], from_logits=self.from_logits_students)
            max_loss = tf.ones_like(batch_losses) * 16.118095
            batch_losses = tf.where(gt_branch == i, batch_losses, max_loss)
            losses.append(tf.reduce_mean(batch_losses))

        if not self.cce_students_only:
            losses.append(self.ensemble_cce_weight * tf.reduce_mean(keras.backend.sparse_categorical_crossentropy(y_true, y_pred[:, self.num_studs, :],
                                                                                                                  from_logits=self.from_logits_ensemble)))
        # print("cce losses", losses)
        loss_sum = tf.reduce_mean(losses)
        return loss_sum


class cce_and_SUBTASK_weight_loss_tf1(keras.losses.Loss):

    def __init__(self, num_studs, from_logits_students, class_branch_map, ensemble_cce_weight=1.0, from_logits_ensemble=True,
                 contribution_w_loss=1.0, name="cce_and_subtask_loss", cce_ensemble_only=False, cce_students_only=False):
        super().__init__(name=name)
        self.w_w_loss = contribution_w_loss
        if cce_ensemble_only:
            self.cce = sparse_categorical_crossentropy_ens_only_tf1(num_studs=num_studs, from_logits_ensemble=from_logits_ensemble,
                                                                    name="scc_ensemble_only")
        else:
            self.cce = sparse_categorical_crossentropy_subtask_multi_output_tf1(num_studs, from_logits_students, ensemble_cce_weight, class_branch_map,
                                                                                "scc_subtask_multi_output",
                                                                                from_logits_ensemble,
                                                                                cce_students_only=cce_students_only)
        self.w_loss = correct_subtask_weight_loss_tf1(class_branch_map=class_branch_map, num_branches=num_studs)

    def call(self, y_true, y_pred):
        #  y_pred = [stud_0, stud_1, stud_2, ensemble, w1_0, w_1, w_2]
        cce_value = self.cce(y_true, y_pred)
        # print("cce_value", cce_value)
        w_loss_value = self.w_loss(y_true, y_pred)
        # print("w_loss_value", w_loss_value)
        # print("----------")
        final_loss_value = cce_value + self.w_w_loss * w_loss_value
        return final_loss_value


class cce_and_PROJECTION_weight_loss_tf1(keras.losses.Loss):

    def __init__(self, class_branch_map, from_logits_ensemble=True, contribution_w_loss=1.0, name="cce_and_projection_weight_loss_tf1"):
        super().__init__(name=name)
        self.w_w_loss = contribution_w_loss
        self.cce = sparse_categorical_crossentropy_ens_only_tf1(num_studs=0, from_logits_ensemble=from_logits_ensemble,
                                                                name="sparse_categorical_crossentropy_ensemble_only")
        self.w_loss = correct_subtask_weight_loss_tf1(class_branch_map=class_branch_map, num_branches=0)

    def call(self, y_true, y_pred):
        #  y_pred = [ensemble, w1_0, w_1, w_2]
        cce_value = self.cce(y_true, y_pred)
        w_loss_value = self.w_loss(y_true, y_pred)
        final_loss_value = cce_value + self.w_w_loss * w_loss_value
        return final_loss_value


def test_subtask_loss():
    table = tf.lookup.StaticHashTable(
        initializer=tf.lookup.KeyValueTensorInitializer(
            keys=tf.constant([4, 5, 1, 7, 3, 8, 9, 0, 2, 6]),
            values=tf.constant([0, 0, 0, 0, 1, 1, 1, 2, 2, 2]),
        ),
        default_value=tf.constant(-1),
    )
    loss = sparse_categorical_crossentropy_subtask_multi_output_tf1(3, False, 1, table)
    y_true = tf.constant([[1], [2]])
    y_pred = tf.constant(np.asarray([
        [[5.0, 0.2, 0.3], [0.4, 0.3, 0.3], [0.1, 0.2, 0.8], [0.41, 0.2, 0.39],
         [0, 0, 0], [0.5, 0.5, 0.5], [0.9, 0.9, 0.9]],
        [[5.0, 0.2, 0.3], [0.4, 0.3, 0.3], [0.1, 0.2, 0.8], [0.41, 0.2, 0.39],
         [0, 0, 0], [0.5, 0.5, 0.5], [0.9, 0.9, 0.9]]
    ], dtype=np.float32))
    loss.call(y_true, y_pred)


def test_student_subtask_loss():
    table = tf.lookup.StaticHashTable(
        initializer=tf.lookup.KeyValueTensorInitializer(
            keys=tf.constant([4, 5, 1, 7, 3, 8, 9, 0, 2, 6]),
            values=tf.constant([0, 0, 0, 0, 1, 1, 1, 2, 2, 2]),
        ),
        default_value=tf.constant(-1),
    )
    y_true = keras.utils.to_categorical([1, 2], num_classes=10)
    y_pred = tf.constant([[-5, 4, 3, 2, 1, -4, -3, -1.2, 1, 0.5, 0],
                          [-5, 4, 10, 2, 1, -4, -3, -1.2, 1, 0.5, 0]])

    loss = StudentSubtaskLoss(0, table, name=None, from_logits=True, num_classes=11, extra_class=True)
    loss_val = loss.call(y_true, y_pred)
    print(loss_val)


def test_gather_fn():
    data = tf.constant([[[1, 1], [2, 2], [3, 3], [31, 31]],
                        [[4, 4], [5, 5], [6, 6], [61, 61]],
                        [[7, 7], [8, 8], [9, 9], [91, 91]]])

    idx = tf.constant([[1, 0, 1, 0],
                       [0, 0, 1, 1],
                       [1, 0, 1, 0]])

    print("data", data.shape)
    print("idx", idx.shape)
    idx = tf.where(idx)
    print("idx_where", idx.shape)
    print("idx", idx)
    data_i = tf.gather_nd(data, idx)
    print("data_i", data_i.shape)
    data_i = tf.reshape(data_i, [3, 2, 2])
    print("data_i reshape", data_i.shape)
    data_i = tf.expand_dims(data_i, axis=0)
    print("data_i expand_dims", data_i.shape)


def test_cce():
    g_pred = tf.constant([12, -14.3, 4.1, 3.5, 3.1, 11, 2.45, -1, -4.0, 3.25, ])
    g_gt = tf.constant([0, 0, 0, 1, 0, 0, 0, 0, 0, 0])

    top_k = tf.math.in_top_k(g_gt, g_pred, 4)

    cc = keras.losses.CategoricalCrossentropy(from_logits=True)
    cc_value = cc(g_gt, g_pred)
    print(cc_value)


if __name__ == "__main__":
    test_student_subtask_loss()
    # test_student_subtask_loss()
    # test_subtask_loss()

from __future__ import print_function

import os

import numpy as np
import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.callbacks import ModelCheckpoint, LearningRateScheduler
from tensorflow.keras.callbacks import ReduceLROnPlateau
from tensorflow.keras.datasets import cifar10
from tensorflow.keras.layers import AveragePooling2D, Input, Flatten
from tensorflow.keras.layers import Dense, Conv2D, BatchNormalization, Activation
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.regularizers import l2

# Training parameters
batch_size = 32  # orig paper trained all networks with batch_size=128
epochs = 200
data_augmentation = True
num_classes = 10

# Subtracting pixel mean improves accuracy
subtract_pixel_mean = True

# Model parameter
# ----------------------------------------------------------------------------
#           |      | 200-epoch | Orig Paper| 200-epoch | Orig Paper| sec/epoch
# Model     |  n   | ResNet v1 | ResNet v1 | ResNet v2 | ResNet v2 | GTX1080Ti
#           |v1(v2)| %Accuracy | %Accuracy | %Accuracy | %Accuracy | v1 (v2)
# ----------------------------------------------------------------------------
# ResNet20  | 3 (2)| 92.16     | 91.25     | -----     | -----     | 35 (---)
# ResNet32  | 5(NA)| 92.46     | 92.49     | NA        | NA        | 50 ( NA)
# ResNet44  | 7(NA)| 92.50     | 92.83     | NA        | NA        | 70 ( NA)
# ResNet56  | 9 (6)| 92.71     | 93.03     | 93.01     | NA        | 90 (100)
# ResNet110 |18(12)| 92.65     | 93.39+-.16| 93.15     | 93.63     | 165(180)
# ResNet164 |27(18)| -----     | 94.07     | -----     | 94.54     | ---(---)
# ResNet1001| (111)| -----     | 92.39     | -----     | 95.08+-.14| ---(---)
# ---------------------------------------------------------------------------
n = 3


# def paper_lr_schedule(epoch):
#     lr = 1e-3
#     if epoch > 120:
#         lr *= 1e-1
#     elif epoch > 80:
#         lr *= 5e-1
#     return lr


def lr_schedule(epoch):
    lr = 1e-3
    # if epoch > 180:
    #     lr *= 0.5e-3
    # elif epoch > 160:
    #     lr *= 1e-3
    # elif epoch > 120:
    #     lr *= 1e-2
    # elif epoch > 80:
    #     lr *= 1e-1

    if epoch > 180:
        lr *= 0.2e-2
    elif epoch > 160:
        lr *= 0.5e-2
    elif epoch > 120:
        lr *= 0.8e-1
    elif epoch > 80:
        lr *= 1e-1
    return lr


def resSep_layer(inputs,
                 num_filters=16,
                 kernel_size=3,
                 strides=1,
                 activation='relu',
                 batch_normalization=True,
                 conv_first=True,
                 l2_w=1e-4,
                 add_l2_w=True):
    kernel_regularizer = l2(l2_w) if add_l2_w else None
    conv = keras.layers.SeparableConvolution2D(num_filters,
                                               kernel_size=kernel_size,
                                               strides=strides,
                                               padding='same',
                                               kernel_initializer='he_normal',
                                               kernel_regularizer=kernel_regularizer)

    x = inputs
    if conv_first:
        x = conv(x)
        if batch_normalization:
            x = BatchNormalization()(x)
        if activation is not None:
            x = Activation(activation)(x)
    else:
        if batch_normalization:
            x = BatchNormalization()(x)
        if activation is not None:
            x = Activation(activation)(x)
        x = conv(x)
    return x


def resnet_layer(inputs,
                 num_filters=16,
                 kernel_size=3,
                 strides=1,
                 activation='relu',
                 batch_normalization=True,
                 conv_first=True,
                 l2_w=1e-4,
                 add_l2_w=True):
    """2D Convolution-Batch Normalization-Activation stack builder

    # Arguments
        inputs (tensor): input tensor from input image or previous layer
        num_filters (int): Conv2D number of filters
        kernel_size (int): Conv2D square kernel dimensions
        strides (int): Conv2D square stride dimensions
        activation (string): activation name
        batch_normalization (bool): whether to include batch normalization
        conv_first (bool): conv-bn-activation (True) or
            bn-activation-conv (False)

    # Returns
        x (tensor): tensor as input to the next layer
    """
    kernel_regularizer = l2(l2_w) if add_l2_w else None
    conv = Conv2D(num_filters,
                  kernel_size=kernel_size,
                  strides=strides,
                  padding='same',
                  kernel_initializer='he_normal',
                  kernel_regularizer=kernel_regularizer
                  )

    x = inputs
    if conv_first:
        x = conv(x)
        if batch_normalization:
            x = BatchNormalization()(x)
        if activation is not None:
            x = Activation(activation)(x)
    else:
        if batch_normalization:
            x = BatchNormalization()(x)
        if activation is not None:
            x = Activation(activation)(x)
        x = conv(x)
    return x


def gate(x, thresh=0.5, epsilon=100):
    x = Activation('sigmoid')(x)
    return 1 - (1.0 / (1.0 + tf.exp((x - thresh) * epsilon)))


def create_resnet_v1_block(layer_nr, res_block, x, l2_w, num_filters):
    strides = 1
    if layer_nr > 0 and res_block == 0:
        strides = 2
    y = resnet_layer(inputs=x,
                     num_filters=num_filters,
                     strides=strides, l2_w=l2_w)
    y = resnet_layer(inputs=y,
                     num_filters=num_filters,
                     activation=None, l2_w=l2_w)
    if layer_nr > 0 and res_block == 0:  # first layer but not first stack
        x = resnet_layer(inputs=x,
                         num_filters=num_filters,
                         kernel_size=1,
                         strides=strides,
                         activation=None,
                         batch_normalization=False,
                         l2_w=l2_w)
    x = keras.layers.add([x, y])
    x = Activation('relu')(x)

    return x


def resnet_v1_branch_projection(x, input_for_compile, current_num_filter, depth, nr_backbone_layers,
                                top=False, output_neurons=None, final_activation=None):
    l2_w = 1e-4
    num_res_blocks = int((depth - 2) / 6)
    max_layers = 3
    final_pooling = 8 if max_layers == 3 else 4

    for layer_nr in range(nr_backbone_layers, max_layers):
        current_num_filter = current_num_filter * 2
        x = create_resnet_v1_layer(num_res_blocks, layer_nr, x, l2_w, current_num_filter)

    projection = Conv2D(x.shape[-1],
                        kernel_size=1,
                        kernel_initializer='he_normal',
                        kernel_regularizer=l2(l2_w))(x)
    if not top:
        final_output = projection
    else:
        pool = AveragePooling2D(pool_size=final_pooling)(projection)
        flatten = Flatten()(pool)
        final_output = Dense(output_neurons, activation=final_activation, kernel_initializer='he_normal')(flatten)

    branch = Model(inputs=input_for_compile, outputs=final_output)
    return branch


def resnet_v1_branch_no_model(x, current_num_filter, depth, nr_backbone_layers, final_activation, output_neurons):
    l2_w = 1e-4
    num_res_blocks = int((depth - 2) / 6)
    max_layers = 3
    final_pooling = 8 if max_layers == 3 else 4

    for layer_nr in range(nr_backbone_layers, max_layers):
        current_num_filter = current_num_filter * 2
        x = create_resnet_v1_layer(num_res_blocks, layer_nr, x, l2_w, current_num_filter)

    x = AveragePooling2D(pool_size=final_pooling)(x)
    y = Flatten()(x)
    outputs = Dense(output_neurons, activation=final_activation, kernel_initializer='he_normal')(y)

    return outputs


def resnet_v1_branch(x, input_for_compile, current_num_filter, depth, nr_backbone_layers, final_activation,
                     output_neurons, name=None, create_model=True):
    l2_w = 1e-4
    num_res_blocks = int((depth - 2) / 6)
    max_layers = 3
    final_pooling = 8 if max_layers == 3 else 4

    for layer_nr in range(nr_backbone_layers, max_layers):
        current_num_filter = current_num_filter * 2
        x = create_resnet_v1_layer(num_res_blocks, layer_nr, x, l2_w, current_num_filter)

    x = AveragePooling2D(pool_size=final_pooling)(x)
    y = Flatten()(x)
    outputs = Dense(output_neurons, activation=final_activation, kernel_initializer='he_normal', name=name)(y)
    if create_model:
        branch = Model(inputs=input_for_compile, outputs=outputs)
    else:
        branch = outputs
    return branch


def resnet_v1_backbone(inputs, depth, layers, l2_w=1e-4):
    num_filters = 16
    num_res_blocks = int((depth - 2) / 6)
    x = resnet_layer(inputs=inputs)
    for stack in range(layers):
        x = create_resnet_v1_layer(num_res_blocks, stack, x, l2_w, num_filters)
        num_filters *= 2
    backbone = Model(inputs=inputs, outputs=x)
    return backbone, int(num_filters / 2)


def create_resnet_v1_layer(num_res_blocks, layer_nr, x, l2_w, num_filters):
    for res_block in range(num_res_blocks):
        x = create_resnet_v1_block(layer_nr, res_block, x, l2_w, num_filters)
    return x


# def get_layer_sequential(use_bn=True, use_relu=True, trainable=True, bn_moments_momentum=0.99, learn_bn=True, num_filters=16, kernel_size=3, strides=1, wd=1e-5):
#     model_sequential = keras.models.Sequential()
#     if use_bn:
#         bn = BatchNormalization(epsilon=1e-5, momentum=bn_moments_momentum, center=learn_bn, scale=learn_bn)
#         model_sequential.add(bn)
#     if use_relu:
#         relu = Activation('relu')
#         model_sequential.add(relu)
#
#     conv = Conv2D(num_filters,
#                   trainable=trainable,
#                   kernel_size=kernel_size,
#                   strides=strides,
#                   padding='same',
#                   kernel_initializer='he_normal',
#                   kernel_regularizer=l2(wd),
#                   use_bias=False)
#     model_sequential.add(conv)
#     return model_sequential
#
#
# def resnet_v1_sequential(inputs, depth, num_classes=10, final_activation=None, skip_layers=0, input_for_compile=None, l2_w=1e-4):
#     num_filters = 16
#     num_res_blocks = int((depth - 2) / 6)
#     model_sequential = keras.models.Sequential()
#     model_sequential.add()
#     for stack in range(3):
#         if stack < skip_layers:
#             num_filters *= 2
#             continue
#         for res_block in range(num_res_blocks):
#             pass

def resnet_v1(inputs, depth, num_classes=10, final_activation=None, skip_layers=0, input_for_compile=None, l2_w=1e-4):
    if (depth - 2) % 6 != 0:
        raise ValueError('depth should be 6n+2 (eg 20, 32, 44 in [a])')
    # Start model definition.
    num_filters = 16
    num_res_blocks = int((depth - 2) / 6)

    x = resnet_layer(inputs=inputs)
    # Instantiate the stack of residual units
    for stack in range(3):
        if stack < skip_layers:
            num_filters *= 2
            continue
        for res_block in range(num_res_blocks):
            strides = 1
            if stack > 0 and res_block == 0:  # first layer but not first stack
                strides = 2  # downsample

            y = resnet_layer(inputs=x,
                             num_filters=num_filters,
                             strides=strides, l2_w=l2_w)
            y = resnet_layer(inputs=y,
                             num_filters=num_filters,
                             activation=None, l2_w=l2_w)
            if stack > 0 and res_block == 0:  # first layer but not first stack
                # linear projection residual shortcut connection to match
                # changed dims
                x = resnet_layer(inputs=x,
                                 num_filters=num_filters,
                                 kernel_size=1,
                                 strides=strides,
                                 activation=None,
                                 batch_normalization=False,
                                 l2_w=l2_w)
            x = keras.layers.add([x, y])
            x = Activation('relu')(x)
        num_filters *= 2

    # Add classifier on top.
    # v1 does not use BN after last shortcut connection-ReLU

    x = AveragePooling2D(pool_size=8)(x)

    y = Flatten()(x)
    if final_activation == "gate":
        activation = gate
    else:
        activation = final_activation
    outputs = Dense(num_classes,
                    activation=activation,
                    kernel_initializer='he_normal')(y)

    # Instantiate model.
    if input_for_compile is not None:
        model = Model(inputs=input_for_compile, outputs=outputs)
    else:
        model = Model(inputs=inputs, outputs=outputs)
    return model


def ressep_v1_unaltered(input_shape, depth, num_classes=10, activation=None, dropout_rate=None, do_wd_schedule=False):
    if (depth - 2) % 6 != 0:
        raise ValueError('depth should be 6n+2 (eg 20, 32, 44 in [a])')
    num_filters = 16
    num_res_blocks = int((depth - 2) / 6)

    inputs = Input(shape=input_shape)
    x = resSep_layer(inputs=inputs, add_l2_w=(not do_wd_schedule))
    for stack in range(3):
        for res_block in range(num_res_blocks):
            strides = 1
            if stack > 0 and res_block == 0:  # first layer but not first stack
                strides = 2
            y = resSep_layer(inputs=x,
                             num_filters=num_filters,
                             strides=strides, add_l2_w=(not do_wd_schedule))
            y = resSep_layer(inputs=y,
                             num_filters=num_filters,
                             activation=None, add_l2_w=(not do_wd_schedule))
            if stack > 0 and res_block == 0:
                x = resSep_layer(inputs=x,
                                 num_filters=num_filters,
                                 kernel_size=1,
                                 strides=strides,
                                 activation=None,
                                 batch_normalization=False, add_l2_w=(not do_wd_schedule))
            x = keras.layers.add([x, y])
            x = Activation('relu')(x)
        num_filters *= 2

    x = AveragePooling2D(pool_size=8)(x)
    y = Flatten()(x)
    if dropout_rate:
        y = tf.keras.layers.Dropout(dropout_rate)(y)
    outputs = Dense(num_classes,
                    activation=activation,
                    kernel_initializer='he_normal')(y)
    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet_v1_unaltered(input_shape, depth, num_classes=10, activation=None, dropout_rate=None, do_wd_schedule=False):
    if (depth - 2) % 6 != 0:
        raise ValueError('depth should be 6n+2 (eg 20, 32, 44 in [a])')
    num_filters = 16
    num_res_blocks = int((depth - 2) / 6)

    inputs = Input(shape=input_shape)
    x = resnet_layer(inputs=inputs, add_l2_w=(not do_wd_schedule))
    for stack in range(3):
        for res_block in range(num_res_blocks):
            strides = 1
            if stack > 0 and res_block == 0:  # first layer but not first stack
                strides = 2
            y = resnet_layer(inputs=x,
                             num_filters=num_filters,
                             strides=strides, add_l2_w=(not do_wd_schedule))
            y = resnet_layer(inputs=y,
                             num_filters=num_filters,
                             activation=None, add_l2_w=(not do_wd_schedule))
            if stack > 0 and res_block == 0:
                x = resnet_layer(inputs=x,
                                 num_filters=num_filters,
                                 kernel_size=1,
                                 strides=strides,
                                 activation=None,
                                 batch_normalization=False, add_l2_w=(not do_wd_schedule))
            x = keras.layers.add([x, y])
            x = Activation('relu')(x)
        num_filters *= 2

    x = AveragePooling2D(pool_size=8)(x)
    y = Flatten()(x)
    if dropout_rate:
        y = tf.keras.layers.Dropout(dropout_rate)(y)
    outputs = Dense(num_classes,
                    activation=activation,
                    kernel_initializer='he_normal')(y)
    model = Model(inputs=inputs, outputs=outputs)
    return model


def resnet_v2_unaltered(input_shape, depth, num_classes=10, activation=None):
    """ResNet Version 2 Model builder [b]

    Stacks of (1 x 1)-(3 x 3)-(1 x 1) BN-ReLU-Conv2D or also known as
    bottleneck layer
    First shortcut connection per layer is 1 x 1 Conv2D.
    Second and onwards shortcut connection is identity.
    At the beginning of each stage, the feature map size is halved (downsampled)
    by a convolutional layer with strides=2, while the number of filter maps is
    doubled. Within each stage, the layers have the same number filters and the
    same filter map sizes.
    Features maps sizes:
    conv1  : 32x32,  16
    stage 0: 32x32,  64
    stage 1: 16x16, 128
    stage 2:  8x8,  256

    # Arguments
        input_shape (tensor): shape of input image tensor
        depth (int): number of core convolutional layers
        num_classes (int): number of classes (CIFAR10 has 10)

    # Returns
        model (Model): Keras model instance
    """
    if (depth - 2) % 9 != 0:
        raise ValueError('depth should be 9n+2 (eg 56 or 110 in [b])')
    # Start model definition.
    num_filters_in = 16
    num_res_blocks = int((depth - 2) / 9)

    inputs = Input(shape=input_shape)
    # v2 performs Conv2D with BN-ReLU on input before splitting into 2 paths
    x = resnet_layer(inputs=inputs,
                     num_filters=num_filters_in,
                     conv_first=True)

    # Instantiate the stack of residual units
    for stage in range(3):
        for res_block in range(num_res_blocks):
            activation = 'relu'
            batch_normalization = True
            strides = 1
            if stage == 0:
                num_filters_out = num_filters_in * 4
                if res_block == 0:  # first layer and first stage
                    activation = None
                    batch_normalization = False
            else:
                num_filters_out = num_filters_in * 2
                if res_block == 0:  # first layer but not first stage
                    strides = 2  # downsample

            # bottleneck residual unit
            y = resnet_layer(inputs=x,
                             num_filters=num_filters_in,
                             kernel_size=1,
                             strides=strides,
                             activation=activation,
                             batch_normalization=batch_normalization,
                             conv_first=False)
            y = resnet_layer(inputs=y,
                             num_filters=num_filters_in,
                             conv_first=False)
            y = resnet_layer(inputs=y,
                             num_filters=num_filters_out,
                             kernel_size=1,
                             conv_first=False)
            if res_block == 0:
                # linear projection residual shortcut connection to match
                # changed dims
                x = resnet_layer(inputs=x,
                                 num_filters=num_filters_out,
                                 kernel_size=1,
                                 strides=strides,
                                 activation=None,
                                 batch_normalization=False)
            x = keras.layers.add([x, y])

        num_filters_in = num_filters_out

    # Add classifier on top.
    # v2 has BN-ReLU before Pooling
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = AveragePooling2D(pool_size=8)(x)
    y = Flatten()(x)
    outputs = Dense(num_classes,
                    activation=activation,
                    kernel_initializer='he_normal')(y)

    # Instantiate model.
    model = Model(inputs=inputs, outputs=outputs)
    return model


if __name__ == "__main__":
    # Model version
    # Orig paper: version = 1 (ResNet v1), Improved ResNet: version = 2 (ResNet v2)

    version = 1

    # Computed depth from supplied model parameter n
    if version == 1:
        depth = n * 6 + 2
    elif version == 2:
        depth = n * 9 + 2

    # Model name, depth and version
    model_type = 'ResNet%dv%d' % (depth, version)

    # Load the CIFAR10 data.
    (x_train, y_train), (x_test, y_test) = cifar10.load_data()

    # Input image dimensions.
    input_shape = x_train.shape[1:]

    # Normalize data.
    x_train = x_train.astype('float32') / 255
    x_test = x_test.astype('float32') / 255

    # If subtract pixel mean is enabled
    if subtract_pixel_mean:
        x_train_mean = np.mean(x_train, axis=0)
        x_train -= x_train_mean
        x_test -= x_train_mean

    print('x_train shape:', x_train.shape)
    print(x_train.shape[0], 'train samples')
    print(x_test.shape[0], 'test samples')
    print('y_train shape:', y_train.shape)

    # Convert class vectors to binary class matrices.
    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)

    if version == 2:
        model = resnet_v2_unaltered(input_shape=input_shape, depth=depth)
    else:
        model = resnet_v1(input_shape=input_shape, depth=depth, final_activation="softmax")

    model.compile(loss='categorical_crossentropy',
                  optimizer=Adam(lr=lr_schedule(0)),
                  metrics=['accuracy'])
    model.summary()
    print(model_type)

    # Prepare model model saving directory.
    save_dir = os.path.join(os.getcwd(), 'saved_models_no_aug')
    model_name = 'cifar10_%s_model.{epoch:03d}.h5' % model_type
    if not os.path.isdir(save_dir):
        os.makedirs(save_dir)
    filepath = os.path.join(save_dir, model_name)

    # Prepare callbacks for model saving and for learning rate adjustment.
    checkpoint = ModelCheckpoint(filepath=filepath,
                                 monitor='val_acc',
                                 verbose=1,
                                 save_best_only=True)

    lr_scheduler = LearningRateScheduler(lr_schedule)

    lr_reducer = ReduceLROnPlateau(factor=np.sqrt(0.1),
                                   cooldown=0,
                                   patience=5,
                                   min_lr=0.5e-6)

    callbacks = [checkpoint, lr_reducer, lr_scheduler]

    # Run training, with or without data augmentation.
    if not data_augmentation:
        print('Not using data augmentation.')
        model.fit(x_train, y_train,
                  batch_size=batch_size,
                  epochs=epochs,
                  validation_data=(x_test, y_test),
                  shuffle=True,
                  callbacks=callbacks)
    else:
        print('Using real-time data augmentation.')
        # This will do preprocessing and realtime data augmentation:
        datagen = ImageDataGenerator(
            # set input mean to 0 over the dataset
            featurewise_center=False,
            # set each sample mean to 0
            samplewise_center=False,
            # divide inputs by std of dataset
            featurewise_std_normalization=False,
            # divide each input by its std
            samplewise_std_normalization=False,
            # apply ZCA whitening
            zca_whitening=False,
            # epsilon for ZCA whitening
            zca_epsilon=1e-06,
            # randomly rotate images in the range (deg 0 to 180)
            rotation_range=0,
            # randomly shift images horizontally
            width_shift_range=0.1,
            # randomly shift images vertically
            height_shift_range=0.1,
            # set range for random shear
            shear_range=0.,
            # set range for random zoom
            zoom_range=0.,
            # set range for random channel shifts
            channel_shift_range=0.,
            # set mode for filling points outside the input boundaries
            fill_mode='nearest',
            # value used for fill_mode = "constant"
            cval=0.,
            # randomly flip images
            horizontal_flip=True,
            # randomly flip images
            vertical_flip=False,
            # set rescaling factor (applied before any other transformation)
            rescale=None,
            # set function that will be applied on each input
            preprocessing_function=None,
            # image data format, either "channels_first" or "channels_last"
            data_format=None,
            # fraction of images reserved for validation (strictly between 0 and 1)
            validation_split=0.0)

        # Compute quantities required for featurewise normalization
        # (std, mean, and principal components if ZCA whitening is applied).
        datagen.fit(x_train)
        steps_per_epoch = x_train.shape[0] / batch_size
        # Fit the model on the batches generated by datagen.flow().
        model.fit_generator(datagen.flow(x_train, y_train, batch_size=batch_size),
                            validation_data=(x_test, y_test),
                            epochs=epochs, verbose=1, workers=4,
                            callbacks=callbacks, steps_per_epoch=steps_per_epoch)

    # Score trained model.
    scores = model.evaluate(x_test, y_test, verbose=1)
    print('Test loss:', scores[0])
    print('Test accuracy:', scores[1])
